package br.com.cesar.tcc2.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import br.com.cesar.tcc2.modelo.Mensagem;
import br.com.cesar.tcc2.modelo.Usuario;

public interface LocalSocialRMI extends Remote {
	public boolean criarSessao(int codigoUsuario, String loginUsuario) throws RemoteException;
	public void atualizarPosicaoUsuario(int codigoUsuario, double valorLatitude, double valorLongitude) throws RemoteException;
	public void desconectarUsuario(int codigoUsuario) throws RemoteException;
	public ArrayList<Mensagem> recuperarMensagensUsuario(int codigoUsuario) throws RemoteException;
	public ArrayList<Usuario> recuperarListaUsuarios(int codigoUsuario, int distanciaLimite) throws RemoteException;
	public void gravarMensagemUsuario(int codigoUsuarioDestino, int codigoUsuarioOrigem, String loginUsuarioOrigem, String mensagem, double distancia) throws RemoteException;
	public void interromperExecucao() throws RemoteException;
}
