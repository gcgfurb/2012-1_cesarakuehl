package br.com.cesar.tcc2.server;

import java.rmi.Naming;

import br.com.cesar.tcc2.server.controle.ControleSessoes;
import br.com.cesar.tcc2.server.rmi.LocalSocialImpl;
import br.com.cesar.tcc2.server.rmi.LocalSocialRMI;

public final class Inicio {

	public static void main(String[] args) throws Exception {
		if( args == null || args.length == 0) {	
			ControleSessoes cs = new ControleSessoes();
							cs.start();
		
			LocalSocialImpl objetoRMI = new LocalSocialImpl();
			Naming.rebind("//localhost/localsocial", objetoRMI);				
		}
		else
			if( args[0].equals("stop") ) {
				LocalSocialRMI interfaceRMI = (LocalSocialRMI) Naming.lookup("//localhost/localsocial");
							   interfaceRMI.interromperExecucao();							   
			}
			else
				System.out.println("Utilize localsocial.jar <null> para iniciar ou localsocial.jar stop para interromper a execu��o");
				
				
	}
}
