package br.com.cesar.tcc2.server.util;

public final class Util {
	
private static final double RAIO_TERRA = 6367.46d;
	
	public static double aplicarFormulaHarversine2(double lat1, double long1, double lat2, double long2) {
		double difLat  = Math.toRadians(lat2 - lat1);
		double difLong = Math.toRadians(long2 - long1);
		
		double lat1Rad = Math.toRadians(lat1);
		double lat2Rad = Math.toRadians(lat2);
		
		double a = Math.sin(difLat/2) * Math.sin(difLat/2) + Math.sin(difLong/2) * Math.sin(difLong/2) * Math.cos(lat1Rad) * Math.cos(lat2Rad);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		
		return c * RAIO_TERRA * 1000;
	}
	
	 public static double aplicarFormulaHarversine(double lat1, double lng1, double lat2, double lng2) {
		    double dLat = Math.toRadians(lat2-lat1);
		    double dLng = Math.toRadians(lng2-lng1);
		    double sindLat = Math.sin(dLat / 2);
		    double sindLng = Math.sin(dLng / 2);
		    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(lat1) * Math.cos(lat2);
		    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		    double dist = RAIO_TERRA * c * 1000;

		    return dist;
	 }
	
}	
