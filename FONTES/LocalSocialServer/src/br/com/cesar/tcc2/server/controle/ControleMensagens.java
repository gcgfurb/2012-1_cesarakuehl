package br.com.cesar.tcc2.server.controle;

import java.util.ArrayList;
import java.util.HashMap;

import br.com.cesar.tcc2.modelo.Mensagem;
import br.com.cesar.tcc2.server.lock.Lock;

public final class ControleMensagens {
	private static final HashMap<Integer,ArrayList<Mensagem>> hashMensagens = new HashMap<Integer,ArrayList<Mensagem>>();
	private static final Lock controleLock = new Lock();
	
	public static void adicionarMensagem(int codigoUsuarioDestino, Mensagem umaMensagem) throws InterruptedException {
		ControleMensagens.controleLock.lock();
		
		ArrayList<Mensagem> listaMensagens = null;
		
		if( ControleMensagens.hashMensagens.containsKey(codigoUsuarioDestino) ) {
			listaMensagens = ControleMensagens.hashMensagens.get(codigoUsuarioDestino);
			listaMensagens.add(umaMensagem);			
		}
		else {
			listaMensagens = new ArrayList<Mensagem>();
			listaMensagens.add(umaMensagem);
			ControleMensagens.hashMensagens.put(codigoUsuarioDestino, listaMensagens);
		}
		
		ControleMensagens.controleLock.unlock();
	}
	
	public static ArrayList<Mensagem> recuperarListaMensagens(int codigoUsuarioSolicitante) throws InterruptedException {
		ArrayList<Mensagem> listaMensagens = null;
		
		if( ControleMensagens.hashMensagens.containsKey(codigoUsuarioSolicitante) ) {
			ControleMensagens.controleLock.lock();
			
			listaMensagens = ControleMensagens.hashMensagens.get(codigoUsuarioSolicitante);
			ControleMensagens.hashMensagens.remove(codigoUsuarioSolicitante);
			ControleMensagens.controleLock.unlock();
		}
				
		return listaMensagens;
	}
	
	
}
