package br.com.cesar.tcc2.server.rmi;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import br.com.cesar.tcc2.modelo.Mensagem;
import br.com.cesar.tcc2.modelo.Sessao;
import br.com.cesar.tcc2.modelo.Usuario;
import br.com.cesar.tcc2.server.controle.ControleMensagens;
import br.com.cesar.tcc2.server.controle.ControleSessoes;
import br.com.cesar.tcc2.server.util.Util;

public class LocalSocialImpl extends UnicastRemoteObject implements LocalSocialRMI {

	public LocalSocialImpl() throws RemoteException {
		super();
	}

	private static final long serialVersionUID = 8118568660679293472L;

	@Override
	public boolean criarSessao(int codigoUsuario, String loginUsuario) throws RemoteException {
		return ControleSessoes.adicionarSessao(new Sessao(codigoUsuario,loginUsuario));
	}

	@Override
	public void atualizarPosicaoUsuario(int codigoUsuario, double valorLatitude, double valorLongitude) throws RemoteException {
		ControleSessoes.atualizarPosicaoUsuario(codigoUsuario, valorLatitude, valorLongitude);
		System.out.println(codigoUsuario + ":" + valorLatitude + ":" + valorLongitude);
	}
	

	@Override
	public void desconectarUsuario(int codigoUsuario) throws RemoteException {
		ControleSessoes.desconectarUsuario(codigoUsuario);
	}

	@Override
	public ArrayList<Mensagem> recuperarMensagensUsuario(int codigoUsuario) throws RemoteException {
		try {
			return ControleMensagens.recuperarListaMensagens(codigoUsuario);
		}
		catch(InterruptedException ie) {
			System.err.println("Erro ao recuperar mensagens do usu�rio " + codigoUsuario + ": " + ie.getMessage());
		}	
		
		return null;
	}
	
	@Override
	public ArrayList<Usuario> recuperarListaUsuarios(int codigoUsuario, int distanciaLimite) throws RemoteException {
		System.out.println(codigoUsuario + ":" + distanciaLimite);
		
		ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();
		ArrayList<Sessao> listaSessoes = ControleSessoes.recuperarSessoes(codigoUsuario);
		
		if( listaSessoes != null && !listaSessoes.isEmpty() ) {
			Sessao sessaoSolicitante = ControleSessoes.recuperarSessaoUsuario(codigoUsuario);
			
			if( sessaoSolicitante != null ) {			
				for( Sessao umaSessao : listaSessoes ) {
					double distanciaEntreUsuarios = Util.aplicarFormulaHarversine(sessaoSolicitante.getValorLatitude(), sessaoSolicitante.getValorLongitude(), umaSessao.getValorLatitude(), umaSessao.getValorLongitude());
					
					System.out.println(codigoUsuario + ":" + umaSessao.getCodigoUsuario() + ":" + distanciaEntreUsuarios);
					
					if( distanciaLimite == 0 || distanciaLimite >= distanciaEntreUsuarios ) {
						Usuario umUsuario = new Usuario(umaSessao.getCodigoUsuario(),umaSessao.getLoginUsuario());
								umUsuario.setDistancia(distanciaEntreUsuarios);
								
						listaUsuarios.add(umUsuario);		
					}					
				}
			}
			else
				System.out.println(codigoUsuario + ": Sess�o nula");
		}
		else
			System.out.println(codigoUsuario + ": Lista sess�es vazia");
		
		return listaUsuarios;
	}
	
	@Override
	public void gravarMensagemUsuario(int codigoUsuarioDestino, int codigoUsuarioOrigem, String loginUsuarioOrigem, String mensagem, double distancia) throws RemoteException {
		try {
			Mensagem umaMensagem = new Mensagem(codigoUsuarioOrigem, loginUsuarioOrigem, mensagem, distancia);
			ControleMensagens.adicionarMensagem(codigoUsuarioDestino, umaMensagem);
		}
		catch(InterruptedException ie) {
			System.err.println("Erro ao gravar mensagem: " + ie.getMessage());
		}
	}
	
	@Override
	public void interromperExecucao() {
		ControleSessoes.executar = false;
		
		try {
			Naming.unbind("//localhost/localsocial");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
