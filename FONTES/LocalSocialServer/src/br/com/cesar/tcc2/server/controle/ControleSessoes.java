package br.com.cesar.tcc2.server.controle;

import java.util.ArrayList;
import java.util.HashMap;

import br.com.cesar.tcc2.modelo.Sessao;

public final class ControleSessoes extends Thread {
	
	public static boolean executar = true;
	
	private static final HashMap<Integer,Sessao> hashSessoes = new HashMap<Integer,Sessao>();
	
	public synchronized static boolean adicionarSessao(Sessao umaSessao) {
		if( hashSessoes.containsKey(umaSessao.getCodigoUsuario()) ) {
			Sessao sessaoExistente = hashSessoes.get(umaSessao.getCodigoUsuario());
			
			if( sessaoExistente.isInativa() || sessaoExistente.isOciosa() ) {
				//System.out.println("ADICIONAR SESSAO: J� existe uma sess�o para o usu�rio " + umaSessao.getLoginUsuario() + " mas est� inativa");
				ControleSessoes.hashSessoes.put(umaSessao.getCodigoUsuario(), umaSessao);
				return true;
			}
			else {
				//System.out.println("ADICIONAR SESSAO: J� existe uma sess�o para o usu�rio " + umaSessao.getLoginUsuario() + " e n�o est� inativa");
				return false;
			}	
		}
		else {
			//System.out.println("ADICIONAR SESSAO: N�o existe uma sess�o para o usu�rio " + umaSessao.getLoginUsuario());
			ControleSessoes.hashSessoes.put(umaSessao.getCodigoUsuario(), umaSessao);
			return true;
		}
	}
	
	public synchronized static void desconectarUsuario(int codigoUsuario) {
		Sessao umaSessao = hashSessoes.get(codigoUsuario);
		
		if( umaSessao != null )
			umaSessao.setInativa(true);
	}
	
	public synchronized static Sessao recuperarSessaoUsuario(int codigoUsuario) {
		return hashSessoes.get(codigoUsuario);
	}
	
	public synchronized static void atualizarPosicaoUsuario(int codigoUsuario, double valorLatitude, double valorLongitude) {
		Sessao umaSessao = hashSessoes.get(codigoUsuario);
				
		if( umaSessao != null ) {
			//System.out.println("ATUALIZA��O POSICAO: O usu�rio " + umaSessao.getLoginUsuario() + " atualizou sua posi��o para: Lat:  " + valorLatitude + " Long: " + valorLongitude);
			umaSessao.setPosicao(valorLatitude, valorLongitude);
		}
		//else
			//System.out.println("ATUALIZA��O POSICAO: O usu�rio com c�digo " + codigoUsuario + " tentou atualizar sua posi��o, mas n�o est� autenticado no sistema");
	}
	
	public synchronized static ArrayList<Sessao> recuperarSessoes(int codigoUsuario) {
		ArrayList<Sessao> listaSessoes = new ArrayList<Sessao>();
		HashMap<Integer,Sessao> hashSessoesClone = new HashMap<Integer,Sessao>(hashSessoes);
		
		for( int codigoUsuarioSessao : hashSessoesClone.keySet() ) {
			Sessao umaSessao = hashSessoesClone.get(codigoUsuarioSessao);
				   umaSessao.updateDataUltimoContato();
			
			if( codigoUsuario != codigoUsuarioSessao && !umaSessao.isInativa() && !umaSessao.isOciosa() )
				listaSessoes.add(umaSessao);
		}
		
		return listaSessoes;
	}
	
	@Override
	public void run() {
		while(executar) {
			HashMap<Integer,Sessao> hashSessoesClone = new HashMap<Integer,Sessao>(hashSessoes);
			
			if( !hashSessoesClone.isEmpty() )
				for( int codigoUsuario : hashSessoesClone.keySet() ) {
					Sessao umaSessao = hashSessoesClone.get(codigoUsuario);
					
					if( umaSessao.getDataUltimoContato() + 60000 < System.currentTimeMillis() )
						umaSessao.setOciosa(true);
				}
			
			try {
				Thread.sleep(1000);
			}
			catch(InterruptedException ie) {
				System.err.println("ControleSessoes - InterruptedException - " + ie.getMessage());
			}
		}
	}
	
}

