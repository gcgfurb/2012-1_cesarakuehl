package br.com.cesar.tcc2.modelo;

import java.io.Serializable;

public final class Sessao implements Serializable {
	
	private static final long serialVersionUID = -651870789602966885L;

	private int codigoUsuario = -1;
	
	private String loginUsuario = null;
	
	private boolean ociosa = true;
	
	private boolean inativa = false;
	
	private long dataUltimoContato = System.currentTimeMillis();
	
	private double valorLatitude = Double.MIN_VALUE;
	
	private double valorLongitude = Double.MIN_VALUE;
	
	public Sessao (int codigoUsuario, String loginUsuario) {
		this.codigoUsuario = codigoUsuario;
		this.loginUsuario  = loginUsuario;
	}
	
	public int getCodigoUsuario() {
		return this.codigoUsuario;
	}
	
	public String getLoginUsuario() {
		return this.loginUsuario;
	}
	
	public long getDataUltimoContato() {
		return this.dataUltimoContato;
	}
	
	public void updateDataUltimoContato() {
		this.dataUltimoContato = System.currentTimeMillis();
	}
	
	public double getValorLatitude() {
		return this.valorLatitude;
	}
	
	public void setPosicao(double valorLatitude, double valorLongitude) {
		this.valorLatitude = valorLatitude;
		this.valorLongitude = valorLongitude;
		this.updateDataUltimoContato();
		this.ociosa = false;
		this.inativa = false;
	}
	
	public double getValorLongitude() {
		return this.valorLongitude;
	}
	
	public boolean isOciosa() {
		return this.ociosa;
	}
	
	public void setOciosa(boolean ociosa) {
		this.ociosa = ociosa;
	}
	
	public boolean isInativa() {
		return this.inativa;
	}
	
	public void setInativa(boolean inativa) {
		this.inativa = inativa;
	}
	
}
