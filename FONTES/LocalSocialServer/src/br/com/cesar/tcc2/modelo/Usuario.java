package br.com.cesar.tcc2.modelo;

import java.io.Serializable;

public class Usuario implements Serializable {

	private static final long serialVersionUID = 3233920660509249025L;

	private int codigo = -1;
	
	private String login = null;
	
	private double distancia = 0d;
	
	public Usuario(int codigo, String login) {
		this.codigo = codigo;
		this.login = login;
	}
	
	public int getCodigo() {
		return this.codigo;
	}
		
	public String getLogin() {
		return this.login;
	}
	
	public double getDistancia() {
		return this.distancia;
	}
	
	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	
}
