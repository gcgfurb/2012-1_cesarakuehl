package br.com.cesar.tcc2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.modelo.Usuario;

public final class UsuarioDAO {
	
	private Connection conn = null;
	
	public UsuarioDAO(Connection conn) {
		this.conn = conn;
	}
	
	public int recuperarCodigoUsuario() throws SQLException {
		int codigo = -1;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.conn.prepareStatement("select ls_sq_usuario.nextval from dual");
			rs = ps.executeQuery();
			
			if( rs.next() )
				codigo = rs.getInt(1);
		}
		finally {
			if( rs != null )
				rs.close();
			
			if( ps != null )
				ps.close();			
		}
		
		return codigo;
	}
	
	public String[] recuperarDadosUsuario(String nmLogin) throws SQLException {
		String[] dados = null;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.conn.prepareStatement("select cd_usuario, ds_senha from ls_usuario where ds_login = ?");
			ps.setString(1, nmLogin);
			
			rs = ps.executeQuery();
			
			if( rs.next() ) {
				dados = new String[2];
				dados[0] = rs.getString(1);
				dados[1] = rs.getString(2);
			}	
		}
		finally {
			if( rs != null )
				rs.close();
			
			if( ps != null )
				ps.close();
		}
		
		return dados;
	}
	
	public String recuperarSenhaUsuario(int codigoUsuario) throws SQLException {
		String senha = null;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.conn.prepareStatement("select ds_senha from ls_usuario where cd_usuario = ?");
			ps.setInt(1, codigoUsuario);
			
			rs = ps.executeQuery();
			
			if( rs.next() )
				senha = rs.getString(1);
		}
		finally {
			if( ps != null )
				ps.close();
		}
		
		return senha;
	}
	
	public void alterarSenhaUsuario(int cdUsuario, String senhaNova) throws SQLException {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement("update ls_usuario set ds_senha = ? where cd_usuario = ?");
			ps.setString(1, senhaNova);
			ps.setInt(2, cdUsuario);
			ps.execute();
		}
		finally {
			if( ps != null ) {
				this.conn.commit();
				ps.close();
			}
		}
	}
	
	public void adicionarUsuario(int cdUsuario, String nmUsuario, String nmLogin, String dsSenha) throws SQLException {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement("insert into ls_usuario (cd_usuario, nm_usuario, ds_login, ds_senha, dt_cadastro) values (?,?,?,?,sysdate)");
			ps.setInt(1, cdUsuario);
			ps.setString(2, nmUsuario);
			ps.setString(3, nmLogin);
			ps.setString(4, dsSenha);
			ps.execute();
		}
		finally {
			if( ps != null ) {
				this.conn.commit();
				ps.close();
			}
		}
	}
	
	public Usuario recuperarUsuario(int codigoUsuario) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		Usuario umUsuario = null;
		
		try {
			ps = this.conn.prepareStatement("select cd_usuario, ds_login, vl_latitude, vl_longitude from ls_usuario where cd_usuario = ?");
			ps.setInt(1, codigoUsuario);
			
			rs = ps.executeQuery();
			
			if( rs.next() ) {
				umUsuario = new Usuario(rs.getInt("cd_usuario"), rs.getString("ds_login"));
			}
		}
		finally {
			if( rs != null )
				rs.close();
			
			if( ps != null )
				ps.close();
		}
		
		return umUsuario;
	}
	
	public void gravarArquivoUsuario(int codigoUsuario, String nomeArquivo, long tamanhoArquivo, String caminhoArquivoExterno, String caminhoArquivoLocal) throws SQLException {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement("insert into ls_arquivos values (?,?,?,?,?,sysdate)");
			ps.setInt(1, codigoUsuario);
			ps.setString(2, nomeArquivo);
			ps.setLong(3, tamanhoArquivo);
			ps.setString(4, caminhoArquivoExterno);
			ps.setString(5, caminhoArquivoLocal);
			ps.execute();
		}
		finally {
			if( ps != null ) {
				this.conn.commit();
				ps.close();				
			}	
		}
	}
	
	public void removerArquivoUsuario(int codigoUsuario, String nomeArquivo) throws SQLException {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement("delete ls_arquivos where cd_usuario = ? and nm_arquivo = ?");
			ps.setInt(1, codigoUsuario);
			ps.setString(2, nomeArquivo);
			ps.execute();
		}
		finally {
			if( ps != null ) {
				this.conn.commit();
				ps.close();
			}	
		}
	}
	
	public String recuperarCaminhoArquivo(int codigoUsuario, String nomeArquivo) throws SQLException {
		String caminhoArquivo = null;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.conn.prepareStatement("select ds_caminho_local from ls_arquivos where cd_usuario = ? and nm_arquivo = ?");
			ps.setInt(1, codigoUsuario);
			ps.setString(2, nomeArquivo);
			
			rs = ps.executeQuery();
			
			if( rs.next() )
				caminhoArquivo = rs.getString(1);
		}
		finally {
			if( rs != null )
				rs.close();
			
			if( ps != null )
				ps.close();
		}
		
		return caminhoArquivo;
	}
	
	public ArrayList<Arquivo> recuperarArquivosUsuario(int codigoUsuario) throws SQLException {
		ArrayList<Arquivo> dadosArquivos = null;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.conn.prepareStatement("select nm_arquivo, vl_arquivo_tam, ds_caminho from ls_arquivos where cd_usuario = ?");
			ps.setInt(1, codigoUsuario);
			
			rs = ps.executeQuery();
			
			while( rs.next() ) {
				if( dadosArquivos == null )
					dadosArquivos = new ArrayList<Arquivo>();
				
				dadosArquivos.add(new Arquivo(rs.getString(1), rs.getLong(2), rs.getString(3)));
			}
		}
		finally {
			if( rs != null )
				rs.close();
			
			if( ps != null )
				ps.close();
		}
		
		return dadosArquivos;
	}
	
	public void fecharConexao() throws SQLException {
		this.conn.close();
	}
}
