package br.com.cesar.tcc2.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class DAOFactory {
	
	public static UsuarioDAO getUsuarioDAO() throws SQLException, ClassNotFoundException {
		return new UsuarioDAO(DAOFactory.getConnection());
	}
	
	private static Connection getConnection() throws SQLException, ClassNotFoundException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
		String hostOracle = System.getProperty("lsOracleHost"),
			   portOracle = System.getProperty("lsOraclePort"),
			   userOracle = System.getProperty("lsOracleUser"),
			   passOracle = System.getProperty("lsOraclePass"),
			   sidOracle  = System.getProperty("lsOracleSID");
		
		String url = "jdbc:oracle:thin:@" + hostOracle + ":" + portOracle + "/" + sidOracle;

		return DriverManager.getConnection(url, userOracle, passOracle);
	}
}
