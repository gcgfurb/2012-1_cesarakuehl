package br.com.cesar.tcc2.ws;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.cesar.tcc2.dao.DAOFactory;
import br.com.cesar.tcc2.dao.UsuarioDAO;
import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.modelo.ArquivoWrapper;
import br.com.cesar.tcc2.modelo.Mensagem;
import br.com.cesar.tcc2.modelo.MensagemWrapper;
import br.com.cesar.tcc2.modelo.Usuario;
import br.com.cesar.tcc2.modelo.UsuarioWrapper;
import br.com.cesar.tcc2.server.rmi.LocalSocialRMI;

public class LocalSocial implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String acao = null;
	
	private LocalSocialRMI conectarLocalSocialServer() throws RemoteException, MalformedURLException, NotBoundException {
		return (LocalSocialRMI) Naming.lookup("//localhost/localsocial");
	}
	
	private void imprimirExcecao(String metodo, String tipoExcecao, String erro) {
		System.err.println("###### EXCE��O NO LOCALSOCIAL ######");
		System.err.println("M�todo: " + metodo);
		System.err.println("Exce��o: " + tipoExcecao);
		System.err.println("Mensagem: " + erro);
		System.err.println("A��o sendo executada: " + this.acao);
		System.err.println("####################################");
	}
	
	private boolean verificarEstruturaPastas(String caminhoPastaUsuario) {
		boolean sucesso = false;
		
		File pastaUsuario = new File(caminhoPastaUsuario);
			
		if( !pastaUsuario.exists() )
			sucesso =  pastaUsuario.mkdir();
		else
			sucesso = true;
			
		return sucesso;	
	}
	
	private void gravarArquivo(String caminho, byte[] buffer) throws IOException {
		BufferedOutputStream bos = null;
		
		try {
			bos = new BufferedOutputStream(new FileOutputStream(caminho));
			bos.write(buffer);
		}
		finally {
			if( bos != null )
				bos.close();
		}
	}
	
	/**
	 * @return -3: O usu�rio j� est� autenticado<br>
	 * 		   -2: Ocorreu uma exce��o na autentica��o do usu�rio<br>
	 *         -1: Usu�rio n�o existe ou senha invalida<br>
	 *       else: Autentica��o realizada com sucesso
	 * 		   
	 */
	
	public int autenticarUsuario(String loginUsuario, String senhaUsuario) {
		int resultado = -2;
		
		try {
			this.acao = "Carregando objeto de UsuarioDAO";
			UsuarioDAO uDAO = DAOFactory.getUsuarioDAO();
			
			this.acao = "Recuperando os dados do usu�rio " + loginUsuario;
			String[] dados = uDAO.recuperarDadosUsuario(loginUsuario);
			
			if( dados == null ) {
				resultado = -1;
				System.out.println("N�o foi encontrado o usu�rio " + loginUsuario);
			}	
			else
				if( dados[1].equals(senhaUsuario) ) {
					resultado = Integer.parseInt(dados[0]);
					System.out.println("Logou om sucesso");
				}	
				else {
					resultado = -1;
					System.out.println("As senhas n�o conferem");
				}	
			
			
			
			this.acao = "Fechando a conex�o com o banco de dados";
			uDAO.fecharConexao();
			
			if( resultado > -1 ) {
				LocalSocialRMI interfaceRMI = this.conectarLocalSocialServer();
				
				if( !interfaceRMI.criarSessao(resultado, loginUsuario) )
					resultado = -3;
			}
		}
		catch(SQLException sqle) {
			this.imprimirExcecao("autenticarUsuario(String loginUsuario, String senhaUsuario)", "SQLException", sqle.getMessage());
			resultado = -2;			
		}
		catch(ClassNotFoundException cnfe) {
			this.imprimirExcecao("autenticarUsuario(String loginUsuario, String senhaUsuario)", "ClassNotFoundException", cnfe.getMessage());
			resultado = -2;			
		}
		catch(NotBoundException nbe) {
			this.imprimirExcecao("autenticarUsuario(String loginUsuario, String senhaUsuario)", "NotBoundException", nbe.getMessage());
			resultado = -2;			
		}
		catch(MalformedURLException mue) {
			this.imprimirExcecao("autenticarUsuario(String loginUsuario, String senhaUsuario)", "MalformedURLException", mue.getMessage());
			resultado = -2;			
		}
		catch(RemoteException re) {
			this.imprimirExcecao("autenticarUsuario(String loginUsuario, String senhaUsuario))", "RemoteException", re.getMessage());
			resultado = -2;			
		}
		
		return resultado;
	}
	
	/**
	 * @return -1: Ocorreu uma exce��o no registro do usu�rio<br>
	 *          0: Usu�rio j� existe<br>
	 *          1: Cadastro realizado com sucesso
	 * 		   
	 */
	
	public int registrarUsuario(String nomeUsuario, String loginUsuario, String senhaUsuario) {
		int resultado = -1;
		
		try {
			this.acao = "Carregando objeto de UsuarioDAO";
			UsuarioDAO uDAO = DAOFactory.getUsuarioDAO();
			
			this.acao = "Verificando se o usu�rio j� est� cadastrado";
			if( uDAO.recuperarDadosUsuario(loginUsuario) != null )
				resultado = 0;
			else {
				this.acao = "Recuperando um novo c�digo de usu�rio";
				int cdUsuario = uDAO.recuperarCodigoUsuario();
				
				this.acao = "Registrando o usu�rio";
				uDAO.adicionarUsuario(cdUsuario, nomeUsuario, loginUsuario, senhaUsuario);
				
				resultado = 1;
			}
				
			this.acao = "Fechando conex�o com o banco de dados";
			uDAO.fecharConexao();		   	   
		}
		catch(SQLException sqle) {
			this.imprimirExcecao("registrarUsuario(String nomeUsuario, String loginUsuario, String senhaUsuario)", "SQLException", sqle.getMessage());
		}
		catch(ClassNotFoundException cnfe) {
			this.imprimirExcecao("registrarUsuario(String nomeUsuario, String loginUsuario, String senhaUsuario)", "ClassNotFoundException", cnfe.getMessage());
		}		
		
		return resultado;
	}
	
	public void atualizarPosicaoUsuario(int codigoUsuario, double valorLatitude, double valorLongitude) {
		try {
			LocalSocialRMI interfaceRMI = this.conectarLocalSocialServer();			
						   interfaceRMI.atualizarPosicaoUsuario(codigoUsuario, valorLatitude, valorLongitude);	
		}
		catch(NotBoundException nbe) {
			this.imprimirExcecao("atualizarPosicaoUsuario(int codigoUsuario, double vlLatitude, double vlLongitude)", "NotBoundException", nbe.getMessage());
		}
		catch(MalformedURLException mue) {
			this.imprimirExcecao("atualizarPosicaoUsuario(int codigoUsuario, double vlLatitude, double vlLongitude)", "MalformedURLException", mue.getMessage());
		}
		catch(RemoteException re) {
			this.imprimirExcecao("atualizarPosicaoUsuario(int codigoUsuario, double vlLatitude, double vlLongitude)", "RemoteException", re.getMessage());
		}
	}
	
	public int alterarSenhaUsuario(int codigoUsuario, String senhaAtual, String senhaNova) {
		int resultado = -2;
		
		try {
			this.acao = "Carregando objeto de UsuarioDAO";
			UsuarioDAO uDAO = DAOFactory.getUsuarioDAO();
			
			this.acao = "Recuperando a senha do usu�rio com c�digo " + codigoUsuario;
			String senha = uDAO.recuperarSenhaUsuario(codigoUsuario);
			
			if( senha.equals(senhaAtual) ) {
				this.acao = "Atualizando a senha do usu�rio com c�digo " + codigoUsuario;
				uDAO.alterarSenhaUsuario(codigoUsuario, senhaNova);
				
				this.acao = "Fechando conex�o com o banco de dados";
				uDAO.fecharConexao();
				
				resultado = 0;
			}
			else
				resultado = -1;			
		}
		catch(SQLException sqle) {
			this.imprimirExcecao("alterarSenhaUsuario(int codigoUsuario, String senhaAtual, String senhaNova)", "SQLException", sqle.getMessage());
			resultado = -2;
		}
		catch(ClassNotFoundException cnfe) {
			this.imprimirExcecao("alterarSenhaUsuario(int codigoUsuario, String senhaAtual, String senhaNova)", "ClassNotFoundException", cnfe.getMessage());
			resultado = -2;
		}
		
		return resultado;
	}
	
	public void desconectarUsuario(int codigoUsuario) {
		try {
			LocalSocialRMI interfaceRMI = this.conectarLocalSocialServer();
						   interfaceRMI.desconectarUsuario(codigoUsuario);				
		}
		catch(NotBoundException nbe) {
			this.imprimirExcecao("desconectarUsuario(int codigoUsuario)", "NotBoundException", nbe.getMessage());
		}
		catch(MalformedURLException mue) {
			this.imprimirExcecao("desconectarUsuario(int codigoUsuario)", "MalformedURLException", mue.getMessage());
		}
		catch(RemoteException re) {
			this.imprimirExcecao("desconectarUsuario(int codigoUsuario)", "RemoteException", re.getMessage());
		}
	}
	
	public UsuarioWrapper recuperarListaUsuarios(int codigoUsuario, int distanciaLimite) {
		System.out.println("INICIO: " + System.currentTimeMillis());
		UsuarioWrapper uw = null;
		
		try {
			LocalSocialRMI interfaceRMI = this.conectarLocalSocialServer();			
			ArrayList<Usuario> listaUsuarios = interfaceRMI.recuperarListaUsuarios(codigoUsuario, distanciaLimite);
			
			if( listaUsuarios != null && !listaUsuarios.isEmpty() ) {
				Usuario[] arrayUsuarios = new Usuario[listaUsuarios.size()];
				
				for( int indice = 0; indice < listaUsuarios.size(); indice++ )
					arrayUsuarios[indice] = listaUsuarios.get(indice);
				
				uw = new UsuarioWrapper();
				uw.setArrayUsuarios(arrayUsuarios);
			}
						   
		}
		catch(NotBoundException nbe) {
			this.imprimirExcecao("recuperarListaUsuarios(int codigoUsuario, int distanciaLimite)", "NotBoundException", nbe.getMessage());
		}
		catch(MalformedURLException mue) {
			this.imprimirExcecao("recuperarListaUsuarios(int codigoUsuario, int distanciaLimite)", "MalformedURLException", mue.getMessage());
		}
		catch(RemoteException re) {
			this.imprimirExcecao("recuperarListaUsuarios(int codigoUsuario, int distanciaLimite)", "RemoteException", re.getMessage());
		}
		
		System.out.println("FIM: " + System.currentTimeMillis());
		return uw;
	}
	
	public void gravarMensagemUsuario(int codigoUsuarioDestino, int codigoUsuarioOrigem, String loginUsuarioOrigem, String mensagem, double distancia) {
		try {
			System.out.println("Conectando ao server");
			LocalSocialRMI interfaceRMI = this.conectarLocalSocialServer();
			
			System.out.println("Enviando mensagem ao server");
						   interfaceRMI.gravarMensagemUsuario(codigoUsuarioDestino, codigoUsuarioOrigem, loginUsuarioOrigem, mensagem, distancia);
						   
			System.out.println("Mensagem enviada");			   
		}
		catch(NotBoundException nbe) {
			this.imprimirExcecao("gravarMensagemUsuario(int codigoUsuarioDestino, int codigoUsuarioOrigem, String loginUsuarioOrigem, String mensagem, double distancia)", "NotBoundException", nbe.getMessage());
		}
		catch(MalformedURLException mue) {
			this.imprimirExcecao("gravarMensagemUsuario(int codigoUsuarioDestino, int codigoUsuarioOrigem, String loginUsuarioOrigem, String mensagem, double distancia)", "MalformedURLException", mue.getMessage());
		}
		catch(RemoteException re) {
			this.imprimirExcecao("gravarMensagemUsuario(int codigoUsuarioDestino, int codigoUsuarioOrigem, String loginUsuarioOrigem, String mensagem, double distancia))", "RemoteException", re.getMessage());
		}
	}
	
	public MensagemWrapper recuperarMensagensUsuario(int codigoUsuario) {
		MensagemWrapper mw = null;
		
		try {
			LocalSocialRMI interfaceRMI = this.conectarLocalSocialServer();
			
			ArrayList<Mensagem> listaMensagens = interfaceRMI.recuperarMensagensUsuario(codigoUsuario);
			
			if( listaMensagens != null && !listaMensagens.isEmpty() ) {
				Mensagem[] arrayMensagens = new Mensagem[listaMensagens.size()];
				
				for( int indice = 0; indice < listaMensagens.size(); indice++ )
					arrayMensagens[indice] = listaMensagens.get(indice);
				
				mw = new MensagemWrapper();
				mw.setArrayMensagem(arrayMensagens);
			}
		}
		catch(NotBoundException nbe) {
			this.imprimirExcecao("recuperarMensagensUsuario(int codigoUsuario)", "NotBoundException", nbe.getMessage());
		}
		catch(MalformedURLException mue) {
			this.imprimirExcecao("recuperarMensagensUsuario(int codigoUsuario)", "MalformedURLException", mue.getMessage());
		}
		catch(RemoteException re) {
			this.imprimirExcecao("recuperarMensagensUsuario(int codigoUsuario)", "RemoteException", re.getMessage());
		}
		
		return mw;
	}
	
	/**
	 * @param codigoUsuario
	 * @param nomeArquivo
	 * @param bufferArquivo
	 * @return -2 Se ocorrer falha no upload, -1 se o arquivo j� existir, 0 se ocorrer com sucesso
	 */
	
	public int uploadArquivoUsuario(int codigoUsuario, String nomeArquivo, byte[] bufferArquivo) {
		int resultado = -2;
		
		String caminhoPastaUsuario   = System.getProperty("lsFolderDir") + File.separator + System.getProperty("lsFolderName") + File.separator + codigoUsuario,
			   caminhoArquivoLocal   = caminhoPastaUsuario + File.separator + nomeArquivo.replace(" ", "_"),
			   caminhoArquivoExterno =  System.getProperty("lsFolderName") + "/" + codigoUsuario + "/" + nomeArquivo.replace(" ", "_");
						
		if( this.verificarEstruturaPastas(caminhoPastaUsuario) ) {
			try {
				this.acao = "Carregando objeto de UsuarioDAO";
				UsuarioDAO uDAO = DAOFactory.getUsuarioDAO();
				
				this.acao = "Verificando se o arquivo " + nomeArquivo + " j� foi vinculado ao usu�rio com c�digo " + codigoUsuario;
				String caminho = uDAO.recuperarCaminhoArquivo(codigoUsuario, nomeArquivo);
				
				if( caminho == null ) {					
					this.acao = "Gravando o arquivo " + nomeArquivo + " para o usu�rio com c�digo " + codigoUsuario;
					this.gravarArquivo(caminhoArquivoLocal, bufferArquivo);
					
					this.acao = "Vinculando o arquivo " + nomeArquivo + " com o usu�rio com c�digo " + codigoUsuario;
					uDAO.gravarArquivoUsuario(codigoUsuario, nomeArquivo, bufferArquivo.length, caminhoArquivoExterno, caminhoArquivoLocal);
					
					resultado = 0;
				}
				else
					resultado = -1;
				
				this.acao = "Fechando conex�o com o banco de dados";
				uDAO.fecharConexao();
			}
			catch(IOException io) {
				this.imprimirExcecao("uploadArquivoUsuario(int codigoUsuario, String nomeArquivo, byte[] bufferArquivo)", "IOException", io.getMessage());
				resultado = -2;
			}
			catch(SQLException sqle) {
				this.imprimirExcecao("uploadArquivoUsuario(int codigoUsuario, String nomeArquivo, byte[] bufferArquivo)", "SQLException", sqle.getMessage());
				resultado = -2;
			}
			catch(ClassNotFoundException cnfe) {
				this.imprimirExcecao("uploadArquivoUsuario(int codigoUsuario, String nomeArquivo, byte[] bufferArquivo)", "ClassNotFoundException", cnfe.getMessage());
				resultado = -2;
			}
		}
		else
			this.imprimirExcecao("uploadArquivoUsuario(int codigoUsuario, String nomeArquivo, byte[] bufferArquivo)", "IOException", "Falha na cria��o da pasta " + caminhoPastaUsuario);
		
		return resultado;
	}
	
	/**
	 * 
	 * @param codigoUsuario
	 * @param nomeArquivo
	 * @return -1 Falha na remo��o, 0 remo��o com sucesso
	 */
	
	public int deletarArquivoUsuario(int codigoUsuario, String nomeArquivo) {
		int resultado = -1;
		
		try {
			this.acao = "Carregando objeto de UsuarioDAO";
			UsuarioDAO uDAO = DAOFactory.getUsuarioDAO();
			
			this.acao = "Recuperando o caminho do arquivo " + nomeArquivo + " do usu�rio com c�digo " + codigoUsuario;
			String caminhoArquivo = uDAO.recuperarCaminhoArquivo(codigoUsuario, nomeArquivo);
			
			this.acao = "Removendo o arquivo " + caminhoArquivo + " do usu�rio com c�digo " + codigoUsuario;					
			File arquivo = new File(caminhoArquivo);
			 	 arquivo.delete();
			 	 
			uDAO.removerArquivoUsuario(codigoUsuario, nomeArquivo);
			
			this.acao = "Fechando a conex�o com o banco de dados";
			uDAO.fecharConexao();
			
			resultado = 0;
		}
		catch(SQLException sqle) {
			this.imprimirExcecao("deletarArquivoUsuario(int codigoUsuario, String nomeArquivo)", "SQLException", sqle.getMessage());
			resultado = -1;
		}
		catch(ClassNotFoundException cnfe) {
			this.imprimirExcecao("deletarArquivoUsuario(int codigoUsuario, String nomeArquivo)", "ClassNotFoundException", cnfe.getMessage());
			resultado = -1;
		}
		
		return resultado;
	}
	
	public ArquivoWrapper recuperarListaArquivosUsuario(int codigoUsuario) {
		ArquivoWrapper aw = null;
		
		try {
			this.acao = "Criando objeto de UsuarioDAO";
			UsuarioDAO uDAO = DAOFactory.getUsuarioDAO();
			
			this.acao = "Recuperando lista de arquivos do usu�rio com c�digo " + codigoUsuario;
			ArrayList<Arquivo> listaArquivos = uDAO.recuperarArquivosUsuario(codigoUsuario);
			
			this.acao = "Fechando conex�o com o banco de dados";
			uDAO.fecharConexao();
			
			if( listaArquivos != null && !listaArquivos.isEmpty() ) {
				Arquivo[] arrayArquivos = new Arquivo[listaArquivos.size()];
				
				for( int indice = 0; indice < listaArquivos.size(); indice++ )
					arrayArquivos[indice] = listaArquivos.get(indice);
				
				aw = new ArquivoWrapper(arrayArquivos);
			}
		}
		catch(SQLException sqle) {
			this.imprimirExcecao("recuperarListaArquivosUsuario(int codigoUsuario)", "SQLException", sqle.getMessage());
		}
		catch(ClassNotFoundException cnfe) {
			this.imprimirExcecao("recuperarListaArquivosUsuario(int codigoUsuario)", "ClassNotFoundException", cnfe.getMessage());
		}
		
		return aw;
	}
	
}
