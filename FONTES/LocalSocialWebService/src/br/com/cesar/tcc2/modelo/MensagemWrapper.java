package br.com.cesar.tcc2.modelo;

public class MensagemWrapper {
	
	private Mensagem[] arrayMensagem = null;
	
	public void setArrayMensagem(Mensagem[] arrayMensagem) {
		this.arrayMensagem = arrayMensagem;
	}
	
	public Mensagem[] getArrayMensagem() {
		return this.arrayMensagem;
	}
	
}
