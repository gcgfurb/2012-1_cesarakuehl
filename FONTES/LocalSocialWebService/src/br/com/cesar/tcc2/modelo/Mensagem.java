package br.com.cesar.tcc2.modelo;

import java.io.Serializable;

public final class Mensagem implements Serializable {

	private static final long serialVersionUID = 2540937528655550165L;

	private int codigoUsuario = -1;
	
	private String loginUsuario = null;
	
	private String mensagem = null;
	
	private double distancia = 0d;
	
	public Mensagem(int codigoUsuario, String loginUsuario, String mensagem, double distancia) {
		this.codigoUsuario = codigoUsuario;
		this.loginUsuario = loginUsuario;
		this.mensagem = mensagem;
		this.distancia = distancia;
	}
	
	public int getCodigoUsuario() {
		return this.codigoUsuario;
	}
	
	public String getLoginUsuario() {
		return this.loginUsuario;
	}
	
	public String getMensagem() {
		return this.mensagem;
	}
	
	public double getDistancia() {
		return this.distancia;
	}	
}
