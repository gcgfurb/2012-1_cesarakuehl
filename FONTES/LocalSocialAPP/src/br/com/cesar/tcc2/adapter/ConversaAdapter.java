package br.com.cesar.tcc2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.cesar.tcc2.R;
import br.com.cesar.tcc2.modelo.Conversa;
import br.com.cesar.tcc2.util.Util;

public class ConversaAdapter extends BaseAdapter {
	
	private Context context = null;
	
	private Conversa[] arrayConversas = null;
	
	public ConversaAdapter(Context context, Conversa[] arrayConversas) {
		this.context = context;
		this.arrayConversas = arrayConversas;
	}
	
	public int getCount() {
		return this.arrayConversas.length;
	}

	public Object getItem(int position) {
		return this.arrayConversas[position];
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Conversa umaConversa = this.arrayConversas[position];
		
		LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View v = inflater.inflate(R.layout.adaptador_conversa, null);
		
		TextView tvUsuario = (TextView) v.findViewById(R.id.adptConvLogin);
				 tvUsuario.setText(umaConversa.getApelidoUsuario());
				 
		TextView tvDistancia = (TextView) v.findViewById(R.id.adptConvDistancia);
				 tvDistancia.setText(Util.formatarDistancia(umaConversa.getDistanciaUsuario()));
		
				 
		String ultimaMensagem = umaConversa.getHistoricoConversa().get(umaConversa.getHistoricoConversa().size() - 1);
		
		if( ultimaMensagem.length() > 50 )
			ultimaMensagem = ultimaMensagem.substring(0, 45) + "...";		
				 
		TextView tvUltimaMensagem = (TextView) v.findViewById(R.id.adptConvUltimaMensagem);
				 tvUltimaMensagem.setText(ultimaMensagem);		
		
		return v;
	}

}
