package br.com.cesar.tcc2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.cesar.tcc2.R;
import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.util.Util;

public class ArquivoAdapter extends BaseAdapter {
	
	private Context context = null;
	
	private Arquivo[] arrayArquivos = null;
	
	public ArquivoAdapter(Context context, Arquivo[] arrayArquivos) {
		this.context = context;
		this.arrayArquivos = arrayArquivos;
	}
	
	public int getCount() {
		if( this.arrayArquivos != null )		
			return this.arrayArquivos.length;
		else
			return 0;
	}

	public Object getItem(int position) {
		return this.arrayArquivos[position];
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Arquivo umArquivo = arrayArquivos[position];
		
		LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View v = inflater.inflate(R.layout.adaptador_arquivo, null);
		
		TextView tvNome = (TextView) v.findViewById(R.id.adptArquivoNome);
				 tvNome.setText(umArquivo.getNome());		 
				 
		TextView tvTamanho = (TextView) v.findViewById(R.id.adptArquivoTamanho);
				 tvTamanho.setText(Util.formatarTamanhoArquivo(umArquivo.getTamanho()));	 
				
		return v;	
	}

}
