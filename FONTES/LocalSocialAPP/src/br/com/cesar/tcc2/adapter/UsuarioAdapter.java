package br.com.cesar.tcc2.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import br.com.cesar.tcc2.AtividadeConversa;
import br.com.cesar.tcc2.R;
import br.com.cesar.tcc2.modelo.Usuario;
import br.com.cesar.tcc2.task.RecuperarArquivosUsuario;
import br.com.cesar.tcc2.util.Util;

public class UsuarioAdapter extends BaseAdapter {
	
	protected static class RowViewHolder {
        public Button botaoChat;
        public Button botaoArquivos;
    }
	
	private Context context = null;
	
	private Usuario[] arrayUsuarios = null;
	
	public UsuarioAdapter(Context context, Usuario[] arrayUsuarios) {
		this.context = context;
		this.arrayUsuarios = arrayUsuarios;
	}
	
	public int getCount() {
		if( this.arrayUsuarios != null )
			return this.arrayUsuarios.length;
		else
			return 0;
	}

	public Object getItem(int arg0) {
		if( this.arrayUsuarios != null )
			return arrayUsuarios[arg0];
		else
			return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final Usuario umUsuario = arrayUsuarios[position];
		
		LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View v = inflater.inflate(R.layout.adaptador_usuario, null);
		
		TextView tvLogin = (TextView) v.findViewById(R.id.adptUsuarioLogin);
				 tvLogin.setText(umUsuario.getLogin());		 
				 
		TextView tvDistancia = (TextView) v.findViewById(R.id.adptUsuarioDistancia);
				 tvDistancia.setText(Util.formatarDistancia(umUsuario.getDistancia()));
				 
		Button btChat = (Button) v.findViewById(R.id.adptUsuarioChat);
			   btChat.setOnClickListener( new OnClickListener() {
				   public void onClick(View v) {
					   Intent it = new Intent(context,AtividadeConversa.class);
					   		  it.putExtra("usuario", umUsuario);
					   		  
					   context.startActivity(it);		  
				   }				   
			   });
		
		Button btArquivos = (Button) v.findViewById(R.id.adptUsuarioArquivos);
			   btArquivos.setOnClickListener( new OnClickListener() {
				   public void onClick(View v) {
					   new RecuperarArquivosUsuario(context,umUsuario).execute();		  
				   }				   
			   });
			   
		RowViewHolder rvh = new RowViewHolder();
				  	  rvh.botaoChat = btChat;
				  	  rvh.botaoArquivos = btArquivos;
				  	  
		v.setTag(rvh);			  
			   
		return v;
	}

}
