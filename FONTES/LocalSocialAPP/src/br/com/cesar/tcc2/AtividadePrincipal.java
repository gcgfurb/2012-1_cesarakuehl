package br.com.cesar.tcc2;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import br.com.cesar.tcc2.adapter.ConversaAdapter;
import br.com.cesar.tcc2.adapter.UsuarioAdapter;
import br.com.cesar.tcc2.modelo.Conversa;
import br.com.cesar.tcc2.modelo.UsuarioWrapper;
import br.com.cesar.tcc2.task.AguardarGPS;
import br.com.cesar.tcc2.task.Logout;
import br.com.cesar.tcc2.task.ProcurarUsuarios;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public class AtividadePrincipal extends Activity {
	
	public static final String BROADCAST_LISTA_USUARIOS= "br.com.cesar.tcc2.service.Sincronizacao.AtualizacaoListaUsuarios";
	
	public static final String BROADCAST_LISTA_CONVERSAS= "br.com.cesar.tcc2.service.Sincronizacao.AtualizacaoListaConversas";
	
	public static final String BROADCAST_ERROS = "br.com.cesar.tcc2.service.Sincronizacao.Erros";
	
	private BroadcastReceiver broadcastReceiverErros = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	Toast.makeText(AtividadePrincipal.this, intent.getStringExtra("erro"), Toast.LENGTH_LONG).show();
        }
	};
	
	private BroadcastReceiver broadcastReceiverListaUsuarios = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	UsuarioWrapper uw = (UsuarioWrapper) intent.getSerializableExtra("listaUsuarios");
        	
        	ListView lv = (ListView) findViewById(R.id.principalLVUsuarios);
        			 lv.setAdapter(new UsuarioAdapter(AtividadePrincipal.this, uw.getArrayUsuarios()));	        		
        }
	};
	
	private BroadcastReceiver broadcastReceiverListaConversas = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	AtividadePrincipal.this.atualizarListaConversas();	        		
        }
	};
	
	public static final int MENU_ARQUIVOS = 0;
	
	public static final int MENU_SENHA = 1;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		
		menu.add(0, MENU_ARQUIVOS, 0, "Meus Arquivos");
		menu.add(0, MENU_SENHA, 0, "Alterar Senha");
		
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch( item.getItemId() ) {
			case MENU_ARQUIVOS: Intent it = new Intent(this, MeusArquivos.class);
								this.startActivity(it);
								return true;

			case MENU_SENHA: Intent it2 = new Intent(this, AtividadeAlteracaoSenha.class);
							 this.startActivity(it2);
						 	 return true;						
		}
		
		return false;
	}
	
	private void procurarUsuarios() {
		ListView lv = (ListView) findViewById(R.id.principalLVUsuarios);
		new ProcurarUsuarios(AtividadePrincipal.this,lv).execute();
	}
	
	private void inicializarServicos() {
		Intent it = new Intent("LSGEO");
 	    startService(it);
 	    
 	    Intent it2 = new Intent("LSSINC");
	    startService(it2);
	}
	
	private void interromperServicos() {
		Intent it = new Intent("LSGEO");
 	    stopService(it);
 	    
 	    Intent it2 = new Intent("LSSINC");
 	    stopService(it2);
	}
	
	private void registrarReceivers() {
		 registerReceiver(this.broadcastReceiverListaUsuarios, new IntentFilter(BROADCAST_LISTA_USUARIOS));
		 registerReceiver(this.broadcastReceiverListaConversas, new IntentFilter(BROADCAST_LISTA_CONVERSAS));
	 	 registerReceiver(this.broadcastReceiverErros, new IntentFilter(BROADCAST_ERROS));
	}
	
	private void desregistrarReceivers() {
		unregisterReceiver(this.broadcastReceiverListaUsuarios);
		unregisterReceiver(this.broadcastReceiverListaConversas);
		unregisterReceiver(this.broadcastReceiverErros);
	}
	
	private void finalizarNotifications() {
		if( !Parametros.listaNotifications.isEmpty() ) {
        	NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        
        	for( int idNotification : Parametros.listaNotifications )
        		nm.cancel(idNotification);
		}
	}
	
	private void finalizarConversas() {
		if( !Parametros.hashConversas.isEmpty() ) {
			LocalSocialWS lsws = new LocalSocialWS();
			
			for( int codigoUsuario : Parametros.hashConversas.keySet() ) {
				Conversa umaConversa = Parametros.hashConversas.get(codigoUsuario);
				
				try {
					lsws.gravarMensagemUsuario(umaConversa.getCodigoUsuario(), Parametros.getCodigoUsuario(), Parametros.getApelidoUsuario(), "/fim", 0D);
				}
				catch(Exception e) {}		
			}
			
			lsws = null;
		}
	}
	
	private void inicializarAbas() {
		final TabHost mTabHost = (TabHost) findViewById(R.id.principalControleAbas);
		  			  mTabHost.setup();
		  			  mTabHost.addTab(mTabHost.newTabSpec("usuarios").setIndicator("Usu�rios").setContent(R.id.principalLVUsuarios));
		  			  mTabHost.addTab(mTabHost.newTabSpec("conversas").setIndicator("Conversas").setContent(R.id.principalLVConversas));
		  			  mTabHost.setCurrentTab(0);
	}
	
	private void inicializarSeekBar() {
		final TextView tv = (TextView) findViewById(R.id.principalTvDistancia);
 		   			   tv.setText("100m");
 		   			   tv.setGravity(Gravity.CENTER);

 		SeekBar sb = (SeekBar) findViewById(R.id.principalSeekBar);
 				sb.setProgress(100);
 				sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener () { 	   				

 					public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
 						if( progress == 0 )
 							tv.setText("Sem limites");
 						else
 							tv.setText(String.valueOf(progress) + "m");

 						tv.setGravity(Gravity.CENTER);
 						Parametros.distanciaComunicacao = progress;
 					}

 					public void onStartTrackingTouch(SeekBar seekBar) {}

 					public void onStopTrackingTouch(SeekBar seekBar) {
 						procurarUsuarios();			    								
 					} 	   				
 				});
	}
	
	private void inicializarBotoes() {
		Button botaoRefresh = (Button) findViewById(R.id.principalBtRefresh);
		   	   botaoRefresh.setOnClickListener(new OnClickListener() {
		   		   public void onClick(View arg0) {
		   			   procurarUsuarios();
		   		   }				   
		   	   });
	}
	
	private void inicializarListViewConversas() {
		ListView lvConversas = (ListView) findViewById(R.id.principalLVConversas);
				 lvConversas.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> arg0, View arg1,	int arg2, long arg3) {
						Conversa umaConversa = (Conversa) arg0.getItemAtPosition(arg2);
						
						Intent it = new Intent(AtividadePrincipal.this, AtividadeConversa.class);
							   it.putExtra("conversa", umaConversa);
							   
						startActivity(it);						
					}					 
				 });		
	}
	
	private void atualizarListaConversas() {
		if( !Parametros.hashConversas.isEmpty() ) {
			ListView lvConversas = (ListView) findViewById(R.id.principalLVConversas);
					 lvConversas.setAdapter(new ConversaAdapter(AtividadePrincipal.this, Util.recuperarArrayConversas()));
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.atividade_principal);
		
		this.inicializarAbas();
		this.inicializarSeekBar();
		this.inicializarBotoes();
		this.inicializarListViewConversas();
		
		this.inicializarServicos();
		this.registrarReceivers();
		
		new AguardarGPS(this).execute(); 
	}
	
	@Override
	public void onResume() {
		super.onResume();
		Parametros.sistemaEmPausa = false;
		
		if( Parametros.sistemaInicializado )
			this.procurarUsuarios();
		
		this.atualizarListaConversas();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		Parametros.sistemaEmPausa = true;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		this.interromperServicos();
		
		new Logout(this,Parametros.getCodigoUsuario()).execute();
				
		this.finalizarNotifications();
        this.finalizarConversas();		
		this.desregistrarReceivers();
	}
	
}
