package br.com.cesar.tcc2;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.com.cesar.tcc2.util.Util;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public final class CadastroUsuario extends Activity implements OnClickListener {
	
	private ProgressDialog progressDialog = null;  
	
	private class RegistroTask extends AsyncTask<Void, Integer, Void> {
		
		private String nome  = null,
					   login = null,
					   senha = null;
		
		private int resultado = -2;
		
		public RegistroTask(String nome, String login, String senha) {
			super();
			this.nome  = nome;
			this.login = login;
			this.senha = senha;
		}
				
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(CadastroUsuario.this,"", "Realizando registro...", false, false);
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			LocalSocialWS lsws = new LocalSocialWS();
			
			try {
				Util util = new Util();
				
				this.senha = util.criptografarSenha(this.senha);
				
				util = null;
				
				this.resultado = lsws.registrarUsuario(this.nome, this.login, this.senha);					
			}
			catch(IOException io) {
				Toast.makeText(CadastroUsuario.this, "IOException: " + io.getMessage(), Toast.LENGTH_LONG).show();
			}
			catch(XmlPullParserException xppe) {
				Toast.makeText(CadastroUsuario.this, "XmlPullParserException: " + xppe.getMessage(), Toast.LENGTH_LONG).show();
			}
			catch(Exception e) {
				Toast.makeText(CadastroUsuario.this, "Exception: " + e.getMessage(), Toast.LENGTH_LONG).show();
			}	
			
			return null;
		}
		
		@Override  
        protected void onPostExecute(Void result) {  
            progressDialog.dismiss();  
            //initialize the View  
            //setContentView(R.layout.main);  
            
            switch( this.resultado ) {
			case -1: Toast.makeText(CadastroUsuario.this, "Ocorreu um erro no servidor do LocalSocial, tente novamente mais tarde", Toast.LENGTH_LONG).show();
					 finish();
					 break;
					 
			case 0: Toast.makeText(CadastroUsuario.this, "J� existe um usu�rio com este login", Toast.LENGTH_LONG).show();
					break;
					
			case 1: Toast.makeText(CadastroUsuario.this, "Cadastro realizado com sucesso! Bem vindo ao LocalSocial!", Toast.LENGTH_LONG).show();
					finish();
					break;									 
            }
        }		
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_registro);
        
        Button btRegistrar = (Button) findViewById(R.id.registrarBtRegistrar);
 	   		   btRegistrar.setOnClickListener(this);
    }
	
	private void cadastrarUsuario() {
		EditText edNome    = (EditText) findViewById(R.id.registrarEdNome),
				 edUsuario = (EditText) findViewById(R.id.registrarEdLogin),
				 edSenha   = (EditText) findViewById(R.id.registrarEdSenha);

		String nmUsuario = edNome.getText().toString(),
			   dsLogin   = edUsuario.getText().toString(),
			   dsSenha   = edSenha.getText().toString();
		
		if( nmUsuario == null || nmUsuario.equals("") ) {
			Toast.makeText(CadastroUsuario.this, "Voc� deve informar o seu nome", Toast.LENGTH_LONG).show();
			edNome.requestFocus();
		}
		else
			if( dsLogin == null || dsLogin.equals("") ) {
				Toast.makeText(CadastroUsuario.this, "Voc� deve informar o seu nome de usu�rio", Toast.LENGTH_LONG).show();
				edUsuario.requestFocus();
			}
			else
				if( dsSenha == null || dsSenha.equals("") ) {
					Toast.makeText(CadastroUsuario.this, "Voc� deve informar a sua senha", Toast.LENGTH_LONG).show();
					edSenha.requestFocus();
				}
				else
					new RegistroTask(nmUsuario,dsLogin,dsSenha).execute();
	}
	
	public void onClick(View v) {
		switch( v.getId() ) {
			case R.id.registrarBtRegistrar: this.cadastrarUsuario();											
				                            break;
		}		
	}	
}
