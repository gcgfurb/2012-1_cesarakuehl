package br.com.cesar.tcc2;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import br.com.cesar.tcc2.adapter.ArquivoAdapter;
import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.task.EnviarArquivo;
import br.com.cesar.tcc2.task.RemoverArquivo;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;

public class MeusArquivos extends Activity implements OnItemClickListener {
	
	public static final String BROADCAST_ATUALIZACAO = "br.com.cesar.tcc2.MeusArquivos.AtualizarLista";
	private static final int DIALOG_LOAD_FILE = 1000;
	
	private String[] mFileList;
	private File mPath = Environment.getExternalStorageDirectory();
	private String mChosenFile;
	
	private ListView lvArquivos = null;
	
	private BroadcastReceiver broadcastReceiverAtualizacao = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	atualizarListaArquivos();	
        }
	};	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.atividade_arquivos);
		
		Button botao = (Button) findViewById(R.id.arquivosBtCompartilharArquivo);
			   botao.setOnClickListener( new OnClickListener() {
				   public void onClick(View v) {
					   onCreateDialog(DIALOG_LOAD_FILE);				
				   }				   
			   });
		
		this.lvArquivos = (ListView) findViewById(R.id.arquivosLVArquivos);
		this.lvArquivos.setOnItemClickListener(this);
			   
		this.atualizarListaArquivos();
		registerReceiver(this.broadcastReceiverAtualizacao, new IntentFilter(BROADCAST_ATUALIZACAO));
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(this.broadcastReceiverAtualizacao);
	}
	
	private void atualizarListaArquivos() {
		this.lvArquivos.setAdapter(new ArquivoAdapter(this,Util.recuperarArrayArquivos()));
	}
	
	private void loadFileList() {
		if( this.mPath.exists() )
			this.mFileList = this.mPath.list();
		else
			this.mFileList= new String[0];
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		Log.v("SELECIONAR_ARQUIVO", "Caminho a ser verificado: " + mPath.getAbsolutePath());
		this.loadFileList();
		Dialog dialog = null;
		AlertDialog.Builder builder = new Builder(this);
		
		if( id == DIALOG_LOAD_FILE ) {
			builder.setTitle("Escolha o arquivo");
			   
			   if( mFileList == null ) {
				   dialog = builder.create();
				   return dialog;
			   }
			   
			   builder.setItems(mFileList, new DialogInterface.OnClickListener(){
				   public void onClick(DialogInterface dialog, int which){
					   mChosenFile = mFileList[which];
					   
					   final File escolhido = new File(mPath.getAbsoluteFile() + File.separator + mChosenFile);
					   
					   if( escolhido.isDirectory() ) {
						   mPath = escolhido;											  
						   MeusArquivos.this.onCreateDialog(DIALOG_LOAD_FILE);										   
					   }
					   else {
						   if( escolhido.length() <= Parametros.LIMITE_TAMANHO_ARQUIVO ) {
							   if( !Parametros.listaArquivosComparilhados.contains(escolhido.getAbsolutePath()) ) {							   
								   AlertDialog.Builder builder = new AlertDialog.Builder(MeusArquivos.this);
	        									   	   builder.setMessage("Deseja enviar o arquivo " + mChosenFile + " para o servidor do LocalSocial?");
	        									   	   builder.setCancelable(false);
	        									   	   builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
	        									   		   public void onClick(DialogInterface dialog, int id) {
	        									   			   new EnviarArquivo(MeusArquivos.this, escolhido).execute();
	        									   		   }	        						
		        									   });
		        					
	        									   	   builder.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
	        									   		   public void onClick(DialogInterface dialog, int id) {
	        									   			   dialog.cancel();
	        									   		   }
	        									   	   });
	        					
	        						AlertDialog alert = builder.create();
	        									alert.show();
							   }
							   else
								   Toast.makeText(MeusArquivos.this, "Este arquivo j� est� compartilhado", Toast.LENGTH_LONG).show();
						   }
						   else
							   Toast.makeText(MeusArquivos.this, "Voc� n�o pode enviar arquivo com mais de 10mb", Toast.LENGTH_LONG).show();
						   
						   mPath = Environment.getExternalStorageDirectory();
					   }					   
				   }
			   });
		}
		
		dialog = builder.show();
		return dialog;
	 }
	
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
		final Arquivo umArquivo = (Arquivo) arg0.getItemAtPosition(position);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(MeusArquivos.this);
	   	   					builder.setMessage("Interromper compartilhamento do arquivo " + umArquivo.getNome() + "?");
	   	   					builder.setCancelable(false);
	   	   					builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
	   	   						public void onClick(DialogInterface dialog, int id) {
	   	   							new RemoverArquivo(MeusArquivos.this, umArquivo).execute();
	   	   						}	        						
	   	   					});

	   	   					builder.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
	   	   						public void onClick(DialogInterface dialog, int id) {
	   	   							dialog.cancel();
	   	   						}
	   	   					});

	   	AlertDialog alert = builder.create();
	   				alert.show();			
	}
	
}
