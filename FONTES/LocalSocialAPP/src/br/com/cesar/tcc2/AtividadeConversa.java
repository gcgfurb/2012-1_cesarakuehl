package br.com.cesar.tcc2;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.cesar.tcc2.modelo.Conversa;
import br.com.cesar.tcc2.modelo.Usuario;
import br.com.cesar.tcc2.task.EnviarMensagem;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;

public class AtividadeConversa extends Activity implements OnClickListener {
	
	public static final String BROADCAST_MENSAGEM = "br.com.cesar.tcc2.AtividadeConversa.NovaMensagem";
	
	public static final String BROADCAST_DISTANCIA = "br.com.cesar.tcc2.AtividadeConversa.AtualizacaoDistancia";
	
	private Conversa umaConversa = null;
	
	private BroadcastReceiver broadcastReceiverMensagem = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	String mensagem = intent.getStringExtra("msg");
        	
        	if( mensagem.equals("/fim") ) {
        		Toast.makeText(AtividadeConversa.this, "O usu�rio desconectou", Toast.LENGTH_LONG).show();
        		Parametros.hashConversas.remove(umaConversa.getCodigoUsuario());
        		finish();
        	}
        	else {
        		atualizarTextViewMensagem(mensagem);
        		atualizarTextViewDistancia(intent.getDoubleExtra("distancia", 0D));
        	}       		
        }
	};
	
	private BroadcastReceiver broadcastReceiverDistancia = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	atualizarTextViewDistancia(intent.getDoubleExtra("distancia", 0D));        	
        }
	};
	
	private void atualizarTextViewDistancia(double distancia) {
		TextView tvDistancia = (TextView) findViewById(R.id.convTvDistancia);
 		 		 tvDistancia.setText(Util.formatarDistancia(distancia));
	}
	
	private void atualizarTextViewMensagem(String mensagem) {
		TextView tvConversa = (TextView) findViewById(R.id.convTvConversa);
				 tvConversa.append(mensagem + "\n");
				 
		ScrollView svConversa = (ScrollView) findViewById(R.id.convSvConversa);
				   svConversa.fullScroll(ScrollView.FOCUS_DOWN);
	}
	
	private void inicializarComponentes() {
		Intent it = getIntent();
		
		if( it.hasExtra("usuario") ) {
			Usuario umUsuario = (Usuario) it.getSerializableExtra("usuario");
			
			if( Parametros.hashConversas.containsKey(umUsuario.getCodigo()) )
				this.umaConversa = Parametros.hashConversas.get(umUsuario.getCodigo());
			else {
				this.umaConversa = new Conversa(umUsuario.getCodigo(), umUsuario.getLogin(), umUsuario.getDistancia());
				Parametros.hashConversas.put(umUsuario.getCodigo(), this.umaConversa);
			}
		}
		else
			if( it.hasExtra("codigoUsuario") )
				this.umaConversa = Parametros.hashConversas.get(it.getIntExtra("codigoUsuario", -1));
			else
				this.umaConversa = (Conversa) it.getSerializableExtra("conversa");
		
		TextView tvUsuario = (TextView) findViewById(R.id.convTvUsuario);
	 	 		 tvUsuario.setText(this.umaConversa.getApelidoUsuario());
	 	 
	 	TextView tvDistancia = (TextView) findViewById(R.id.convTvDistancia);
	 	 		 tvDistancia.setText(Util.formatarDistancia(this.umaConversa.getDistanciaUsuario()));
	 	 
	 	TextView tvConversa = (TextView) findViewById(R.id.convTvConversa);		  
	 	 		  
	 	if( this.umaConversa.getHistoricoConversa() != null && !this.umaConversa.getHistoricoConversa().isEmpty() )	 
	 		for( int indice = 0; indice < this.umaConversa.getHistoricoConversa().size(); indice++ )
	 			 if( indice == 0 )
	 				 tvConversa.setText(this.umaConversa.getHistoricoConversa().get(indice) + "\n");
	 			 else
	 				tvConversa.append(this.umaConversa.getHistoricoConversa().get(indice) + "\n");
	 	
	 	Button botaoEnviar = (Button) findViewById(R.id.convBtEnviar);
		   	   botaoEnviar.setOnClickListener(this);
		   	   
		   	   
		// Desabilita o teclado, at� que o usu�rio solicite
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_conversa);
		
		this.inicializarComponentes();	 	
	 	this.umaConversa.setAtiva(true);
	 	
	 	registerReceiver(this.broadcastReceiverMensagem, new IntentFilter(BROADCAST_MENSAGEM)); 
	 	registerReceiver(this.broadcastReceiverDistancia, new IntentFilter(BROADCAST_DISTANCIA)); 	 
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		this.umaConversa.setAtiva(false);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		unregisterReceiver(this.broadcastReceiverMensagem);
		unregisterReceiver(this.broadcastReceiverDistancia);
		this.umaConversa.setAtiva(false);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(this.broadcastReceiverMensagem, new IntentFilter(BROADCAST_MENSAGEM));
		registerReceiver(this.broadcastReceiverDistancia, new IntentFilter(BROADCAST_DISTANCIA)); 	 
		this.umaConversa.setAtiva(true);
	}
	
	public void onClick(View v) {
		EditText etMensagem = (EditText) findViewById(R.id.convEtMensagem);
		String mensagem = etMensagem.getText().toString();
		
		if( mensagem == null || mensagem.equals("") ) {
			Toast.makeText(AtividadeConversa.this, "Digite uma mensagem", Toast.LENGTH_LONG).show();
			etMensagem.requestFocus();
		}
		else {
			while( mensagem.startsWith("/") )			
				mensagem = mensagem.substring(1);
			
			new EnviarMensagem(this, this.umaConversa, mensagem).execute();
			this.atualizarTextViewMensagem(Parametros.getApelidoUsuario() + ": " + mensagem);
			etMensagem.setText("");
			etMensagem.clearFocus();
		}	
	}
		
	
}
