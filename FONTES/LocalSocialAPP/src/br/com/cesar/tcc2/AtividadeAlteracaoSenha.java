package br.com.cesar.tcc2;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.com.cesar.tcc2.task.AlterarSenha;

public class AtividadeAlteracaoSenha extends Activity implements OnClickListener {
	
	public static final String BROADCAST_RESULTADO = "br.com.cesar.tcc2.AtividadeAlteracaoSenha.Resultado";
	
	private BroadcastReceiver broadcastReceiverResultado = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	String resultado = intent.getStringExtra("resultado");
        	
        	if( resultado.equals("") ) {
        		Toast.makeText(AtividadeAlteracaoSenha.this, "Senha alterada com sucesso", Toast.LENGTH_LONG).show();
        		finish();
        	}
        	else
        		Toast.makeText(AtividadeAlteracaoSenha.this, resultado, Toast.LENGTH_LONG).show();
        }
	};
	
	private String[] validarInformacoes() {
		String[] senhas = null;
		
		EditText etSenhaAtual = (EditText) findViewById(R.id.altSenhaAtual);
		
		if( etSenhaAtual.getText().toString() == null || etSenhaAtual.getText().toString().equals("") ){
			Toast.makeText(this, "Voc� deve informar sua senha atual", Toast.LENGTH_LONG).show();
			etSenhaAtual.requestFocus();
		}
		else {
			EditText etSenhaNova   = (EditText) findViewById(R.id.altSenhaNova),
					 etSenhaNovaRP = (EditText) findViewById(R.id.altSenhaNovaRp);
			
			if( etSenhaNova.getText().toString() == null || etSenhaNova.getText().toString().equals("") ) {
				Toast.makeText(this, "Voc� deve informar uma nova senha", Toast.LENGTH_LONG).show();
				etSenhaNova.requestFocus();
			}
			else
				if( etSenhaNovaRP.getText().toString() == null || etSenhaNovaRP.getText().toString().equals("") ) {
					Toast.makeText(this, "Voc� deve repetir a nova senha", Toast.LENGTH_LONG).show();
					etSenhaNovaRP.requestFocus();
				}
				else
					if( !etSenhaNovaRP.getText().toString().equals(etSenhaNova.getText().toString()) ) {
						Toast.makeText(this, "As senhas novas n�o conferem", Toast.LENGTH_LONG).show();
						etSenhaNova.requestFocus();
					}
					else {
						senhas = new String[2];
						senhas[0] = etSenhaAtual.getText().toString();
						senhas[1] = etSenhaNovaRP.getText().toString();
					}						
		}
		
		return senhas;
	}
	
	private void configurarBotoes() {
		Button btCancelar = (Button) findViewById(R.id.altBtCancelar);
			   btCancelar.setOnClickListener(this);
			   
		Button btOK = (Button) findViewById(R.id.altBtOK);
			   btOK.setOnClickListener(this);	   
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alteracao_senha);
		this.configurarBotoes();
		registerReceiver(this.broadcastReceiverResultado, new IntentFilter(BROADCAST_RESULTADO));
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(this.broadcastReceiverResultado);
	}
	
	public void onClick(View v) {
		switch( v.getId() ) {
			case R.id.altBtCancelar: finish();
									 break;
									 
			case R.id.altBtOK: String[] senhas = validarInformacoes();
			   
			   				   if( senhas != null )
			   					   new AlterarSenha(AtividadeAlteracaoSenha.this, senhas[0], senhas[1]).execute();
							   break;
		}
	}
}
