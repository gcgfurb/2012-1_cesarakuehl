package br.com.cesar.tcc2.task;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import br.com.cesar.tcc2.util.Parametros;

public class DownloadArquivo extends TaskPadrao {
	
	private String nomeUsuario = null,
				   nomeArquivo = null,
				   urlArquivo  = null,
				   urlLocal    = null,
				   erro        = null;
	
	
	public DownloadArquivo(Context umContexto, String urlArquivo, String nomeArquivo, String nomeUsuario) {
		super(umContexto, null, "Realizando o download do arquivo");
		this.urlArquivo  = urlArquivo;
		this.nomeUsuario = nomeUsuario;
		this.nomeArquivo = nomeArquivo;
	}
	
	private boolean validarEstruturaPastas() {
		boolean sucesso = false;
		
		File pastaLocalSocial = new File(Parametros.CAMINHO_PASTA_APLICACAO),
			 pastaDownloads   = new File(Parametros.CAMINHO_PASTA_DOWNLOADS),
			 pastaUsuario     = new File(Parametros.CAMINHO_PASTA_DOWNLOADS + File.separator + this.nomeUsuario);		
		
		if( pastaLocalSocial.exists() ) {			
			if( pastaDownloads.exists() ) {
				if( pastaUsuario.exists() )
					sucesso = true;
				else
					sucesso = pastaUsuario.mkdir();
			}
			else {
				if( pastaDownloads.mkdir() )
					sucesso = pastaUsuario.mkdir();		
			}
		}
		else
			if( pastaLocalSocial.mkdir() )
				if( pastaDownloads.mkdir() )
					sucesso = pastaUsuario.mkdir();			
		
		return sucesso;
	}
	
	private ByteArrayBuffer downloadArquivo() throws IOException {
		ByteArrayBuffer bab = null;
		
		URL urlArquivo = new URL(this.urlArquivo);
		
		URLConnection urlConn = urlArquivo.openConnection();
		BufferedInputStream bis = new BufferedInputStream(urlConn.getInputStream());
		
		bab = new ByteArrayBuffer(50);
		int current = 0;
		
		while( (current = bis.read()) != -1 )
			bab.append((byte) current);
		
		bis.close();
		
		return bab;
	}
	
	private void gravarArquivoSD(ByteArrayBuffer bab) throws IOException {
		this.urlLocal = Parametros.CAMINHO_PASTA_DOWNLOADS + File.separator + this.nomeUsuario + File.separator + this.nomeArquivo;
		
		File arquivo = new File(this.urlLocal);
		
		if( arquivo.exists() )
			arquivo.delete();		
		
		FileOutputStream fos = new FileOutputStream(arquivo);
        				 fos.write(bab.toByteArray());
         				 fos.close();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		long inicio = System.currentTimeMillis();
		
		if( this.validarEstruturaPastas() ) {
			try {			
				ByteArrayBuffer bab = this.downloadArquivo();
				
				if( bab != null )
					this.gravarArquivoSD(bab);
			}
			catch(IOException io) {
				this.erro = "IOException - " + io.getMessage();
			}
		}
		else
			this.erro = "N�o foi possivel criar a estrutura de pastas para a recep��o do arquivo";
		
		Log.v("DownloadArquivo", String.valueOf(System.currentTimeMillis() - inicio));
		
		return null;
	}
	
	@Override
    protected void onPostExecute(Void result) {  
		super.progressDialog.dismiss();
		
		if( this.erro != null )
			Toast.makeText(super.umContexto, "Falha no download do arquivo: " + this.erro, Toast.LENGTH_LONG).show();
		else
			Toast.makeText(super.umContexto, "Download realizado com sucesso, o arquivo est� em: " + this.urlLocal, Toast.LENGTH_LONG).show();
	}
	

}
