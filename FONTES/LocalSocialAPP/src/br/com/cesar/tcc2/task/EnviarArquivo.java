package br.com.cesar.tcc2.task;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import br.com.cesar.tcc2.MeusArquivos;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public final class EnviarArquivo extends TaskPadrao {
	
	private File arquivoAEnviar = null;
	
	private String erro = null;
	
	private int resultado = 0;
	
	public EnviarArquivo(Context umContexto, File arquivoAEnviar) {
		super(umContexto,null, "Enviando o arquivo...");
		this.arquivoAEnviar = arquivoAEnviar;
	}
	
	private byte[] carregarArquivo() throws IOException {
		byte[] bufferArquivo = null;
		BufferedInputStream bis = null;
		
		try {
			bufferArquivo = new byte[Integer.parseInt(String.valueOf(this.arquivoAEnviar.length()))];
			
			bis = new BufferedInputStream(new FileInputStream(this.arquivoAEnviar));
			bis.read(bufferArquivo);			
		}
		finally {
			if( bis != null )
				bis.close();
		}
		
		return bufferArquivo;		
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		try {
			byte[] bufferArquivo = this.carregarArquivo();
			
			LocalSocialWS lsws = new LocalSocialWS();
						  
			this.resultado = lsws.uploadArquivoUsuario(Parametros.getCodigoUsuario(), this.arquivoAEnviar.getName(), bufferArquivo);
		}
		catch(ClientProtocolException cpe) {
			this.erro = "ClientProtocolException: " + cpe.getMessage();
		}
		catch(XmlPullParserException xppe) {
			this.erro = "XmlPullParserException: " + xppe.getMessage();
		}
		catch(IOException io) {
			this.erro = "IOException: " + io.getMessage();
		}
		catch(Exception e) {
			this.erro = "Exception: " + e.getMessage();
		}
		
		return null;
	}
	
	@Override
    protected void onPostExecute(Void result) {  
		this.progressDialog.dismiss();
		
		if( this.erro != null )
			Toast.makeText(super.umContexto, "Falha ao enviar arquivo: " + this.erro, Toast.LENGTH_LONG).show();
		else
			switch( this.resultado ) {
				case 0: Toast.makeText(super.umContexto, "Arquivo enviado com sucesso", Toast.LENGTH_LONG).show();
						
						Parametros.listaArquivosComparilhados.add(this.arquivoAEnviar.getAbsolutePath());
						
						try {
							Util.gravarListaArquivos();
							
							Intent it = new Intent(MeusArquivos.BROADCAST_ATUALIZACAO);
							super.umContexto.sendBroadcast(it);							
						}
						catch(IOException io) {
							Toast.makeText(super.umContexto, "Falha ao gravar arquivo de arquivos compartilhados: " + io.getMessage(), Toast.LENGTH_LONG).show();
						}
						catch(Exception e) {
							Toast.makeText(super.umContexto, "Falha ao gravar arquivo de arquivos compartilhados: " + e.getMessage(), Toast.LENGTH_LONG).show();
						}
						
						break;
						
				case -1: Toast.makeText(super.umContexto, "O arquivo j� foi enviado previamente", Toast.LENGTH_LONG).show();
						 break;
						 
				case -2: Toast.makeText(super.umContexto, "Ocorreu um erro no servidor do LocalSocial, tente novamente mais tarde", Toast.LENGTH_LONG).show();
				 		 break;						
			}			
	}	
}
