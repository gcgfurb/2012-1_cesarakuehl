package br.com.cesar.tcc2.task;

import br.com.cesar.tcc2.util.Parametros;
import android.content.Context;

public final class AguardarGPS extends TaskPadrao {

	public AguardarGPS(Context umContexto) {
		super(umContexto, null, "Recuperando sua posi��o");
	}

	@Override
	protected Void doInBackground(Void... params) {
		while( Parametros.getDataUltimaAtualizacaoPosicao() == 0L ) {
			try {
				Thread.sleep(500);
			}
			catch(InterruptedException ie) {
				
			}			
		}
		
		Parametros.sistemaInicializado = true;
		
		return null;
	}
}
