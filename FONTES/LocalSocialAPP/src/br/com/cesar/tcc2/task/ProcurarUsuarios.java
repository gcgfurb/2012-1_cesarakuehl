package br.com.cesar.tcc2.task;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;
import br.com.cesar.tcc2.adapter.UsuarioAdapter;
import br.com.cesar.tcc2.modelo.Usuario;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public class ProcurarUsuarios extends TaskPadrao {
	
	private String erro = null;
	
	private Usuario[] arrayUsuarios = null;
	
	private ListView listView = null;
	
	private boolean sincronizandoPeloServico = false;
	
	public ProcurarUsuarios(Context umContexto, ListView listView) {
		super(umContexto,null,"Procurando usu�rios...");
		this.listView = listView;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		LocalSocialWS lsws = new LocalSocialWS();
		
		try {
			if( !Parametros.sincronizacaoEmAndamento ) {
				Log.v("Acesso", String.valueOf(System.currentTimeMillis()));
				Parametros.sincronizacaoEmAndamento = true;
				this.arrayUsuarios = lsws.recuperarListaUsuarios(Parametros.getCodigoUsuario(), Parametros.distanciaComunicacao);
				Parametros.atualizarDataSincronizacaoUsuarios();
				Parametros.sincronizacaoEmAndamento = false;
				Log.v("Acesso", String.valueOf(System.currentTimeMillis()));
			}
			else {
				this.sincronizandoPeloServico = true;
				
				while( Parametros.sincronizacaoEmAndamento )
					Thread.sleep(100);
			}
		}
		catch(ClientProtocolException cpe) {
			this.erro = "ClientProtocolException: " + cpe.getMessage();
			Parametros.sincronizacaoEmAndamento = false;
		}
		catch(XmlPullParserException xppe) {
			this.erro = "XmlPullParserException: " + xppe.getMessage();
			Parametros.sincronizacaoEmAndamento = false;
		}
		catch(IOException io) {
			this.erro = "IOException: " + io.getMessage();
			Parametros.sincronizacaoEmAndamento = false;
		}
		catch(InterruptedException ie) {
			this.erro = "InterruptedException: " + ie.getMessage();
		}
		catch(Exception e) {
			this.erro = "Exception: " + e.getMessage();
			Parametros.sincronizacaoEmAndamento = false;
		}
		
		return null;
	}
	
	@Override  
    protected void onPostExecute(Void result) {  
		this.progressDialog.dismiss();
		
		//if( this.erro != null )
		//	Toast.makeText(super.umContexto, "Erro ao recuperar lista de usu�rios: " + this.erro, Toast.LENGTH_LONG).show();
		//else
		if( this.erro == null)
			if( !this.sincronizandoPeloServico )
				if( this.arrayUsuarios != null )
					this.listView.setAdapter(new UsuarioAdapter(super.umContexto, this.arrayUsuarios));
				else
					this.listView.setAdapter(new UsuarioAdapter(super.umContexto, null));
	} 
}
