package br.com.cesar.tcc2.task;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import br.com.cesar.tcc2.MeusArquivos;
import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public final class RemoverArquivo extends TaskPadrao {
	
	private Arquivo umArquivo = null;
	private String erro = null;
	
	private int resultado = 0;
	
	public RemoverArquivo(Context umContexto, Arquivo umArquivo) {
		super(umContexto, "", "Removendo arquivo do servidor do LocalSocial...");
		
		this.umArquivo = umArquivo;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		try {
			LocalSocialWS lsws = new LocalSocialWS();
		    this.resultado = lsws.deletarArquivoUsuario(Parametros.getCodigoUsuario(), this.umArquivo.getNome());
		    lsws = null;						  
		}
		catch(ClientProtocolException cpe) {
			this.erro = "ClientProtocolException: " + cpe.getMessage();
		}
		catch(XmlPullParserException xppe) {
			this.erro = "XmlPullParserException: " + xppe.getMessage();
		}
		catch(IOException io) {
			this.erro = "IOException: " + io.getMessage();
		}
		catch(Exception e) {
			this.erro = "Exception: " + e.getMessage();
		}
		
		return null;
	}
	
	@Override
    protected void onPostExecute(Void result) {  
		this.progressDialog.dismiss();
		
		if( this.erro != null )
			Toast.makeText(super.umContexto, "Falha ao remover compartilhamento do arquivo: " + this.erro, Toast.LENGTH_LONG).show();
		else
			switch( this.resultado ) {
				case 0: Toast.makeText(super.umContexto, "O arquivo n�o � mais compartilhado", Toast.LENGTH_LONG).show();
						
						Parametros.listaArquivosComparilhados.remove(umArquivo.getCaminho());
						
						try {
							Util.gravarListaArquivos();
							
							Intent it = new Intent(MeusArquivos.BROADCAST_ATUALIZACAO);
							super.umContexto.sendBroadcast(it);							
						}
						catch(IOException io) {
							Toast.makeText(super.umContexto, "Falha ao gravar arquivo de arquivos compartilhados: " + io.getMessage(), Toast.LENGTH_LONG).show();
						}
						catch(Exception e) {
							Toast.makeText(super.umContexto, "Falha ao gravar arquivo de arquivos compartilhados: " + e.getMessage(), Toast.LENGTH_LONG).show();
						}
						
						break;
						 
				default: Toast.makeText(super.umContexto, "Ocorreu um erro no servidor do LocalSocial, tente novamente mais tarde", Toast.LENGTH_LONG).show();
				 		 break;						
			}			
	}	
	
}
