package br.com.cesar.tcc2.task;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import br.com.cesar.tcc2.ArquivosUsuario;
import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.modelo.ArquivoWrapper;
import br.com.cesar.tcc2.modelo.Usuario;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public class RecuperarArquivosUsuario extends TaskPadrao {
	
	private Usuario umUsuario = null;
	private String erro = null;
	private Arquivo[] arrayArquivos = null;
	
	
	public RecuperarArquivosUsuario(Context umContexto, Usuario umUsuario) {
		super(umContexto, "", "Recuperando lista de arquivos do usuario...");
		this.umUsuario = umUsuario;
		
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		try {
			LocalSocialWS lsws = new LocalSocialWS();
			
			this.arrayArquivos = lsws.recuperarListaArquivosUsuario(umUsuario.getCodigo());
		}
		catch(ClientProtocolException cpe) {
			this.erro = "ClientProtocolException: " + cpe.getMessage();
		}
		catch(XmlPullParserException xppe) {
			this.erro = "XmlPullParserException: " + xppe.getMessage();
		}
		catch(IOException io) {
			this.erro = "IOException: " + io.getMessage();
		}
		catch(Exception e) {
			this.erro = "Exception: " + e.getMessage();
		}
		
		return null;
	}
	
	@Override
    protected void onPostExecute(Void result) {  
		super.progressDialog.dismiss();
		
		if( this.erro != null )
			Toast.makeText(super.umContexto, "Falha ao recuperar lista de arquivos do usuario: " + this.erro, Toast.LENGTH_LONG).show();
		else
			if( this.arrayArquivos == null || this.arrayArquivos.length == 0 )
				Toast.makeText(super.umContexto, "O usu�rio n�o est� compartilhando nenhum arquivo", Toast.LENGTH_LONG).show();
			else {
				Intent it = new Intent(super.umContexto, ArquivosUsuario.class);
					   it.putExtra("usuario", this.umUsuario.getLogin());
					   it.putExtra("arquivos", new ArquivoWrapper(this.arrayArquivos));
					   
				super.umContexto.startActivity(it);					   
			}		
	}
}
