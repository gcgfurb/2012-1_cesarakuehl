package br.com.cesar.tcc2.task;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.Intent;
import br.com.cesar.tcc2.AtividadeAlteracaoSenha;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public final class AlterarSenha extends TaskPadrao {
	
	private String senhaAtual = null;
	private String senhaNova  = null;
	private String erro = null;
	private int resultado = 0;
	
	public AlterarSenha(Context umContexto, String senhaAtual, String senhaNova) {
		super(umContexto, "", "Alterando senha...");
		this.senhaAtual = senhaAtual;
		this.senhaNova  = senhaNova;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		try {
			Util util = new Util();
			this.senhaAtual = util.criptografarSenha(this.senhaAtual);
			this.senhaNova  = util.criptografarSenha(this.senhaNova);
			
			LocalSocialWS lsws = new LocalSocialWS();
			this.resultado = lsws.alterarSenhaUsuario(Parametros.getCodigoUsuario(), this.senhaAtual, this.senhaNova);
		}
		catch(ClientProtocolException cpe) {
			this.erro = "ClientProtocolException: " + cpe.getMessage();
		}
		catch(XmlPullParserException xppe) {
			this.erro = "XmlPullParserException: " + xppe.getMessage();
		}
		catch(IOException io) {
			this.erro = "IOException: " + io.getMessage();
		}
		catch(Exception e) {
			this.erro = "Exception: " + e.getMessage();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.progressDialog.dismiss();
		String mensagemResultado = "";
		
		if( this.erro != null )
			mensagemResultado = "Falha na altera��o da senha: " + this.erro;
		else
			switch( this.resultado ) {
				case 0: break;
				
				case -1: mensagemResultado = "A senha atual n�o est� correta";
						 break;
						 
				case -2: mensagemResultado = "N�o foi possivel alterar sua senha devido um erro no servidor do LocalSocial, tente novamente mais tarde.";
						 break;
			}
		
		Intent it = new Intent(AtividadeAlteracaoSenha.BROADCAST_RESULTADO);
			   it.putExtra("resultado", mensagemResultado);
			   
		super.umContexto.sendBroadcast(it);	   
	}
}
