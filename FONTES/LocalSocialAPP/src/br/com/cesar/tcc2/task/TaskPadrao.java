package br.com.cesar.tcc2.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class TaskPadrao extends AsyncTask<Void, Integer, Void> {
	
	protected ProgressDialog progressDialog = null;
	
	protected Context umContexto;
	
	private String titulo = "",
				   acao   = "";
	
	public TaskPadrao(Context umContexto, String titulo, String acao) {
		this.umContexto = umContexto;
		this.titulo = titulo;
		this.acao = acao;
	}
			
	@Override
	protected void onPreExecute() {
		this.progressDialog = ProgressDialog.show(umContexto, this.titulo, this.acao, false, false);
	}
	
	@Override
	protected abstract Void doInBackground(Void... params);
	
	@Override  
    protected void onPostExecute(Void result) {  
		this.progressDialog.dismiss();
	}    
	
}
