package br.com.cesar.tcc2.task;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import br.com.cesar.tcc2.AtividadePrincipal;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public class Login extends TaskPadrao {
	
	private String login = null,
				   senha = null,
				   erro  = "";
	
	private int resultado = -2;
	
	public Login (Context umContexto, String login, String senha) {
		super(umContexto, "", "Autenticando...");
		this.login = login;
		this.senha = senha;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		LocalSocialWS lsws = new LocalSocialWS();
		
		try {
			Util util = new Util();
			this.senha = util.criptografarSenha(this.senha);
			
			this.resultado = lsws.autenticarUsuario(this.login, this.senha);
		}
		catch(ClientProtocolException cpe) {
			this.erro = "ClientProtocolException: " + cpe.getMessage();
		}
		catch(XmlPullParserException xppe) {
			this.erro = "XmlPullParserException: " + xppe.getMessage();
		}
		catch(IOException io) {
			this.erro = "IOException: " + io.getMessage();
		}
		catch(Exception e) {
			this.erro = "Exception: " + e.getMessage();
		}
		
		return null;
	}
	
	@Override
    protected void onPostExecute(Void result) {  
        super.progressDialog.dismiss();  
        
        switch( this.resultado ) {
        	case -3: Toast.makeText(super.umContexto, "Este usu�rio j� est� autenticado em outro dispositivo", Toast.LENGTH_LONG).show();
			 		 break;	
        				
        	case -2: Toast.makeText(super.umContexto, "Ocorreu um erro no servidor do LocalSocial, tente novamente mais tarde", Toast.LENGTH_LONG).show();
        			 Log.e("Login",erro);
        			 break;
        			 
        	case -1: Toast.makeText(super.umContexto, "Usu�rio ou senha invalida", Toast.LENGTH_LONG).show();
        			 break;
        			 
        	default: Parametros.setCodigoUsuario(this.resultado);
				 	 Parametros.setApelidoUsuario(this.login);
			
				 	 //Intent it = new Intent(super.umContexto, Principal.class);
				 	 Intent it = new Intent(super.umContexto, AtividadePrincipal.class);
				 	 super.umContexto.startActivity(it);
				 	 break;
        }
    }	
}
