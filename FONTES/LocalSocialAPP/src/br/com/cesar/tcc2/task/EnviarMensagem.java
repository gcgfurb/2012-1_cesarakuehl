package br.com.cesar.tcc2.task;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import br.com.cesar.tcc2.modelo.Conversa;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public final class EnviarMensagem extends TaskPadrao {
	
	private String mensagem  = null;
	
	private Conversa umaConversa = null;
	
	private String erro = null;
	
	public EnviarMensagem (Context umContexto, Conversa umaConversa, String mensagem) {
		super(umContexto, null, null);
		this.umaConversa = umaConversa;
		this.mensagem = mensagem;
	}
	
	@Override
	protected void onPreExecute() {}
		
	@Override
	protected Void doInBackground(Void... params) {
		try {
			Log.v("Conversa", "Tentando envia mensagem");
			LocalSocialWS lsws = new LocalSocialWS();
						  lsws.gravarMensagemUsuario(this.umaConversa.getCodigoUsuario(), Parametros.getCodigoUsuario(), Parametros.getApelidoUsuario(), this.mensagem, this.umaConversa.getDistanciaUsuario());
			Log.v("Conversa", "Mensagem enviada");			  
		}
		catch(IOException io) {
			this.erro = "Falha ao enviar mensagem: IOException - " + io.getMessage();
		}
		catch(XmlPullParserException xppe) {
			this.erro = "Falha ao enviar mensagem: XmlPullParserException - " + xppe.getMessage();
		}		
		
		return null;
	}
	
	@Override 
	protected void onPostExecute(Void result) {
		if( this.erro == null )
			this.umaConversa.getHistoricoConversa().add(Parametros.getApelidoUsuario() + ": " + this.mensagem);
		else
			Toast.makeText(super.umContexto, this.erro, Toast.LENGTH_LONG).show();
	}	
}
