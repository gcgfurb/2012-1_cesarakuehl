package br.com.cesar.tcc2.task;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Log;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public final class Logout extends TaskPadrao {
	
	private int codigoUsuario = -1;
	
	public Logout (Context umContexto, int codigoUsuario) {
		super(umContexto, "", "Desconectando...");
		this.codigoUsuario = codigoUsuario;
	}
	
	@Override
	protected void onPreExecute() {}
	
	@Override
	protected Void doInBackground(Void... params) {
		LocalSocialWS lsws = new LocalSocialWS();
		
		try {
			lsws.desconectarUsuario(this.codigoUsuario);
		}
		catch(ClientProtocolException cpe) {
			Log.e("Logout", "ClientProtocolException: " + cpe.getMessage());
		}
		catch(XmlPullParserException xppe) {
			Log.e("Logout", "XmlPullParserException: " + xppe.getMessage());
		}
		catch(IOException io) {
			Log.e("Logout", "IOException: " + io.getMessage());
		}
		catch(Exception e) {
			Log.e("Logout", "Exception: " + e.getMessage());
		}
		
		return null;
	}
	
	@Override  
    protected void onPostExecute(Void result) {} 
}
