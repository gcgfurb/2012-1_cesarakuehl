package br.com.cesar.tcc2.ws;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.modelo.Mensagem;
import br.com.cesar.tcc2.modelo.Usuario;
import br.com.cesar.tcc2.util.Parametros;

public final class LocalSocialWS {
	
	private final String URL = "http://" + Parametros.urlWebService + ":" + Parametros.portaWebService + "/axis2/services/localsocial";
	
	private final String NAMESPACE = "http://ws.tcc2.cesar.com.br";
	
	private SoapObject getSoapObject(String metodo) {
		SoapObject soap = new SoapObject(this.NAMESPACE, metodo);
		
		return soap;
	}
	
	private Object getResponse(SoapObject soap) throws IOException, XmlPullParserException {
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		  						  envelope.setOutputSoapObject(soap);
	
		HttpTransportSE httpTransport = new HttpTransportSE(this.URL,30000);
		 				httpTransport.call("", envelope);
					    					    
		return envelope.getResponse();		    
	}
	
	private Object getResponse(SoapObject soap, MarshalDouble md) throws IOException, XmlPullParserException {
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		  						  envelope.setOutputSoapObject(soap);
	
		md.register(envelope);  						  
		  						  
		HttpTransportSE httpTransport = new HttpTransportSE(this.URL,30000);
		 				httpTransport.call("", envelope);
			
		return envelope.getResponse();		    
	}
	
	private Object getResponse(SoapObject soap, MarshalBase64 md) throws IOException, XmlPullParserException {
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		  						  envelope.setOutputSoapObject(soap);
	
		md.register(envelope);  						  
		  						  
		HttpTransportSE httpTransport = new HttpTransportSE(this.URL,30000);
		 				httpTransport.call("", envelope);
			
		return envelope.getResponse();		    
	}
	
	public int autenticarUsuario(String loginUsuario, String senhaUsuario) throws IOException, XmlPullParserException {
		SoapObject soap = this.getSoapObject("autenticarUsuario");
				   soap.addProperty("loginUsuario", loginUsuario);
				   soap.addProperty("senhaUsuario", senhaUsuario);
				   		
		Object autenticacao = this.getResponse(soap); 
					  
		return Integer.parseInt(autenticacao.toString());
	}
	
	public int registrarUsuario(String nomeUsuario, String loginUsuario, String senhaUsuario) throws IOException, XmlPullParserException {
		SoapObject soap = this.getSoapObject("registrarUsuario");
			 	   soap.addProperty("nomeUsuario", nomeUsuario);
		   		   soap.addProperty("loginUsuario", loginUsuario);
		   		   soap.addProperty("senhaUsuario", senhaUsuario);
		   		
		Object autenticacao = this.getResponse(soap); 

		return Integer.parseInt(autenticacao.toString());
	}
	
	public void atualizarPosicaoUsuario(int codigoUsuario, double valorLatitude, double valorLongitude) throws IOException, XmlPullParserException {
		SoapObject soap = this.getSoapObject("atualizarPosicaoUsuario");
	 	   		   soap.addProperty("codigoUsuario", codigoUsuario);
	 	   		   soap.addProperty("valorLatitude", valorLatitude);
	 	   		   soap.addProperty("valorLongitude", valorLongitude);
		
		   this.getResponse(soap, new MarshalDouble()); 
	}
	
	public Usuario[] recuperarListaUsuarios(int codigioUsuario, int distanciaLimite) throws IOException, XmlPullParserException {
		Usuario[] arrayUsuarios = null;
		
		SoapObject soap = this.getSoapObject("recuperarListaUsuarios");
	 	   		   soap.addProperty("codigoUsuario", codigioUsuario);
	 	   		   soap.addProperty("distanciaLimite", distanciaLimite);
	 	
	 	SoapObject sobj = (SoapObject) this.getResponse(soap);
	 	
	 	if( sobj != null ) {
	 		int qtdeUsuarios = sobj.getPropertyCount();
	 		arrayUsuarios = new Usuario[qtdeUsuarios];
	 		
	 		for( int indice = 0; indice < qtdeUsuarios; indice++ ) {
	 			SoapObject umObjeto = (SoapObject) sobj.getProperty(indice);
	 			
	 			if( umObjeto != null ) {	 	
		 			int codigo = Integer.parseInt(umObjeto.getProperty(0).toString());
		 			
		 			double dist = 0d;
		 			
		 			if( umObjeto.getProperty(1) != null )
		 				dist = Double.parseDouble(umObjeto.getPropertyAsString(1));
		 			
		 			String login = umObjeto.getPropertyAsString(2);
		 			
		 			Usuario umUsuario = new Usuario(codigo,login);
		 					umUsuario.setDistancia(dist);
		 					
		 			arrayUsuarios[indice] = umUsuario;	 			
	 			}
	 			else {
	 				arrayUsuarios = null;
	 				break;
	 			}	
	 		}
	 	}
	 	
	 	return arrayUsuarios;
	}
	
	public void desconectarUsuario(int cdUsuario) throws IOException, XmlPullParserException {
		SoapObject soap = this.getSoapObject("desconectarUsuario");
	 	   		   soap.addProperty("codigoUsuario", cdUsuario);
	 	   		    	   		   
	 	this.getResponse(soap);
	}
	
	public int alterarSenhaUsuario(int cdUsuario, String senhaAtual, String senhaNova) throws IOException, XmlPullParserException {
		int resultado = -2;
		
		SoapObject soap = this.getSoapObject("alterarSenhaUsuario");
 		   		   soap.addProperty("codigoUsuario", cdUsuario);
 		   		   soap.addProperty("senhaAtual", senhaAtual);
 		   		   soap.addProperty("senhaNova", senhaNova);
 		    	   		   
 		Object umObjeto = this.getResponse(soap);
 		
 		resultado = Integer.parseInt(umObjeto.toString());
 		
 		return resultado;   
	}
	
	public void gravarMensagemUsuario(int codigoUsuarioDestino,  int codigoUsuarioOrigem, String loginUsuarioOrigem, String mensagem, double distancia) throws IOException, XmlPullParserException {
		SoapObject soap = this.getSoapObject("gravarMensagemUsuario");
 		   		   soap.addProperty("codigoUsuarioDestino", codigoUsuarioDestino);
 		   		   soap.addProperty("codigoUsuarioOrigem", codigoUsuarioOrigem);
 		   		   soap.addProperty("loginUsuarioOrigem", loginUsuarioOrigem);
 		   		   soap.addProperty("mensagem", mensagem);
 		   		   soap.addProperty("distancia", distancia);
 		   		   
 		 this.getResponse(soap, new MarshalDouble());  		   
	}
	
	public Mensagem[] recuperarMensagensUsuario(int codigoUsuario) throws IOException, XmlPullParserException {
		Mensagem[] arrayMensagens = null;
		
		SoapObject soap = this.getSoapObject("recuperarMensagensUsuario");
 		   		   soap.addProperty("codigoUsuario", codigoUsuario);

 		SoapObject sobj = (SoapObject) this.getResponse(soap);
		
 		if( sobj != null ) {
 			int qtdeMensagens = sobj.getPropertyCount();
 			arrayMensagens = new Mensagem[qtdeMensagens];
 			
 			for( int indice = 0; indice < qtdeMensagens; indice++ ) {
 				SoapObject umObjeto = (SoapObject) sobj.getProperty(indice);
 			
 				if( umObjeto != null ) {
 					int codigoUsuarioEnvio = Integer.parseInt(umObjeto.getPropertyAsString(0));
 					double distancia = Double.parseDouble(umObjeto.getPropertyAsString(1));
 					String login     = umObjeto.getPropertyAsString(2),
 						   mensagem  = umObjeto.getPropertyAsString(3);

 					arrayMensagens[indice] = new Mensagem(codigoUsuarioEnvio, login, mensagem, distancia);
 				}
 				else {
 					arrayMensagens = null;
 					break;
 				}	
 			} 			
 		}
 		
 		return arrayMensagens;
	}
	
	public int uploadArquivoUsuario(int codigoUsuario, String nomeArquivo, byte[] bufferArquivo) throws IOException, XmlPullParserException {
		int resultado = -2;
		
		SoapObject soap = this.getSoapObject("uploadArquivoUsuario");
 		   		   soap.addProperty("codigoUsuario", codigoUsuario);
 		   		   soap.addProperty("nomeArquivo", nomeArquivo);
 		   		   soap.addProperty("bufferArquivo", bufferArquivo);
 		   		   
 		Object retorno = this.getResponse(soap, new MarshalBase64());
 		
 		resultado = Integer.parseInt(retorno.toString());
		
		return resultado;
	}
	
	public int deletarArquivoUsuario(int codigoUsuario, String nomeArquivo) throws IOException, XmlPullParserException {
		int resultado = -1;
		
		SoapObject soap = this.getSoapObject("deletarArquivoUsuario");
 		   		   soap.addProperty("codigoUsuario", codigoUsuario);
 		   		   soap.addProperty("nomeArquivo", nomeArquivo);
 		   		   
 		Object retorno = this.getResponse(soap);
 		 		
 		resultado = Integer.parseInt(retorno.toString());   		   
		
		return resultado;
	}
	
	public Arquivo[] recuperarListaArquivosUsuario(int codigoUsuario) throws IOException, XmlPullParserException {
		Arquivo[] arrayArquivos = null;
		
		SoapObject soap = this.getSoapObject("recuperarListaArquivosUsuario");
 		   		   soap.addProperty("codigoUsuario", codigoUsuario);
 		
 		SoapObject sobj = (SoapObject) this.getResponse(soap);
 		
 		if( sobj != null ) {
 			int qtdeArquivos = sobj.getPropertyCount();
 			arrayArquivos = new Arquivo[qtdeArquivos];
 			
 			for( int indice = 0; indice < qtdeArquivos; indice++ ) {
 				SoapObject umObjeto = (SoapObject) sobj.getProperty(indice);
 			
 				if( umObjeto != null ) {
 					String caminho = umObjeto.getPropertyAsString(0);
 					String nome    = umObjeto.getPropertyAsString(1);
 					long   tamanho = Long.parseLong(umObjeto.getPropertyAsString(2));
 						   
 					arrayArquivos[indice] = new Arquivo(nome,tamanho,caminho);
 				}
 				else {
 					arrayArquivos = null;
 					break;
 				}	
 			} 			
 		}
		
		return arrayArquivos;
	}
	
}
