package br.com.cesar.tcc2;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.com.cesar.tcc2.task.Login;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;

public class LoginUsuario extends Activity implements OnClickListener {
	
	private final int CODIGO_REQUISICAO_GPS = 0;
	
	/**
	 * Verifica se o GPS do dispositivo est� ativo
	 */
	
	private void verificarGPS() {
		LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

	    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        					builder.setMessage("O LocalSocial precisa utilizar o seu dispositivo GPS, deseja ativ�-lo agora?");
	        					builder.setCancelable(false);
	        					builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
	        						public void onClick(DialogInterface dialog, int id) {
	        							startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), CODIGO_REQUISICAO_GPS);
	                                }
	        					});
	        					
	        					builder.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
	        						public void onClick(DialogInterface dialog, int id) {
	        							dialog.cancel();
	        							finish();
	        						}
	        					});
	        					
	        AlertDialog alert = builder.create();
	        			alert.show();
	    }
	}
	
	private void inicializarComponentes() {
		 Button btLogin = (Button) findViewById(R.id.loginBtLogin);
  	   			btLogin.setOnClickListener(this);
  	   
  	   	 Button btRegistrar = (Button) findViewById(R.id.loginBtRegistrar);
  	   	 		btRegistrar.setOnClickListener(this);
  	   
  	   	 Button btConfig = (Button) findViewById(R.id.loginBtConfig);
  	   	 		btConfig.setOnClickListener(this);
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		this.verificarGPS();
		
		Parametros.hashConversas.clear();
		Parametros.listaArquivosComparilhados.clear();
		
        super.onCreate(savedInstanceState);       
        setContentView(R.layout.tela_login);        
        
       	this.inicializarComponentes();
       	
       	try {
       		Util.carregarArquivoParametros();
       		Util.carregarArquivoCompartilhados();
       	}
       	catch(IOException io) {
       		Toast.makeText(LoginUsuario.this, "Falha no carregamento do arquivo de parametros: " + io.getMessage(), Toast.LENGTH_LONG).show();
       	}
       	catch(Exception e) {
       		Toast.makeText(LoginUsuario.this, "Falha no carregamento do arquivo de parametros: " + e.getMessage(), Toast.LENGTH_LONG).show();
       	}
    }
	
	private void realizarAutenticacao() {
		EditText edLogin = (EditText) findViewById(R.id.loginEdUsuario),
				 edSenha = (EditText) findViewById(R.id.loginEdSenha);
						   
		String dsLogin = edLogin.getText().toString(),
			   dsSenha = edSenha.getText().toString();
		
		if( dsLogin == null || dsLogin.equals("") ) {
			Toast.makeText(LoginUsuario.this, "Voc� deve informar seu nome de usu�rio", Toast.LENGTH_LONG).show();
			edLogin.requestFocus();
		}
		else
			if( dsSenha == null  || dsSenha.equals("") ) {
				Toast.makeText(LoginUsuario.this, "Voc� deve informar sua senha", Toast.LENGTH_LONG).show();
				edSenha.requestFocus();
			}
			else
				new Login(LoginUsuario.this, dsLogin,dsSenha).execute();
	}
	
	public void onDestroy() {
		super.onDestroy();
		
		Intent itStop = new Intent("LSGEO");
		stopService(itStop);
	}
	
	public void onClick(View v) {
		switch( v.getId() ) {
			case R.id.loginBtRegistrar: Intent it = new Intent(this, CadastroUsuario.class);
										startActivity(it);
										break;
										
			case R.id.loginBtLogin:		this.realizarAutenticacao();				
									    break;										

			case R.id.loginBtConfig:    Intent itConf = new Intent(this, Configuracoes.class);
										startActivity(itConf);
										break;
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if( requestCode == CODIGO_REQUISICAO_GPS && resultCode == 0 )
			this.verificarGPS();		
	}
	
}