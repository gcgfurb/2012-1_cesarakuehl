package br.com.cesar.tcc2.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import android.os.Environment;
import android.util.Log;
import br.com.cesar.tcc2.modelo.Conversa;


public final class Parametros {
	
	/*
	 * Parametros padr�es imutaveis
	 */
	
	public static final int PORTA_WEBSERVICE = 8080;
	
	public static final String URL_WEBSERVICE = "192.168.0.185";
	
	public static final String CAMINHO_PASTA_APLICACAO = Environment.getExternalStorageDirectory().toString() + File.separator + "LocalSocial";
	
	public static final String CAMINHO_PASTA_DOWNLOADS = Environment.getExternalStorageDirectory().toString() + File.separator + "LocalSocial" + File.separator + "downloads";
	
	public static final String CAMINHO_ARQUIVO_CONFIGURACAO = Environment.getExternalStorageDirectory().toString() + File.separator + "LocalSocial" + File.separator + "parametros.conf";
	
	public static final String CAMINHO_ARQUIVO_COMPARTILHADOS = Environment.getExternalStorageDirectory().toString() + File.separator + "LocalSocial" + File.separator + "arquivos.comp";
	
	public static final long LIMITE_TAMANHO_ARQUIVO = 10485760L;
	
	/*
	 * Outros parametros
	 */
	
	/**
	 * Define se o sistema j� est� apto a come�ar suas atividades<br>
	 * Para que seja verdadeiro � necess�rio recuperar a coordenada do usu�rio
	 */
	
	public static boolean sistemaInicializado = false;
	
	/**
	 * Define se o sistema est� em pausa ou n�o.<br>
	 * O sistema estar� em pausa se o usu�rio n�o estiver na tela principal ou se estiver com o sistema executando em segundo plano.<br>
	 * O sistema estando em pausa implica na n�o sincroniza��o da lista de usu�rios.
	 */
	
	public static boolean sistemaEmPausa = false;
	
	/**
	 * Define se a sincroniza��o com o web-service est� ocorrendo
	 */
	
	public static boolean sincronizacaoEmAndamento = false;
	
	/**
	 * Identifica o c�digo do usu�rio no sistema
	 */
	
	private static int codigoUsuario = Integer.MIN_VALUE;
	
	/**
	 * Identifica o apelido do usu�rio no sistema
	 */
	
	private static String apelidoUsuario = null;	
	
	/**
	 * URL utilizada para contato com o WEBService do LocalSocial
	 */
	
	public static String urlWebService = Parametros.URL_WEBSERVICE;
	
	/**
	 * Porta utilizada para contato com o WEBService do LocalSocial
	 */
	
	public static int portaWebService = Parametros.PORTA_WEBSERVICE;
	
	/**
	 * Define quem atualizou a posi��o do usu�rio, sendo:<br>
	 * 0 - SATELITE <br>
	 * 1 - NETWORK
	 */
	
	private static int origemAtualizacaoPosicao = -1;
	
	/**
	 * Define a data da ultima atualiza��o da posi��o do usu�rio
	 */
	
	private static long dataUltimaAtualizacaoPosicao = 0L;
	
	/**
	 * Define a data da ultima sincroniza��o dos usu�rios
	 */
	
	private static long dataSincronizacaoUsuarios = 0L;
	
	/**
	 * Define a data da ultima sincroniza��o das mensagens
	 */
	
	private static long dataSincronizacaoMensagens = 0L;
	
	/**
	 * Latitude atual do usu�rio
	 */
	
	private static double latitudeUsuario = 0.0d;
	
	/**
	 * Longitude atual do usu�rio
	 */
	
	private static double longitudeUsuario = 0.0d;
	
	/**
	 * Qual a dist�ncia m�xima dos usu�rios ao qual o usu�rio quer se comunicar
	 */
	
	public static int distanciaComunicacao = 100;
	
	/**
	 * Lista de conversas que o usu�rio est� realizando
	 */
	
	public static final HashMap<Integer,Conversa> hashConversas = new HashMap<Integer,Conversa>();
	
	/**
	 * Lista de notifica��es que dever� ser canceladas no termino da execu��o
	 */
	
	public static final ArrayList<Integer> listaNotifications = new ArrayList<Integer>();
	
	/**
	 * Lista dos arquivos que o usu�rio compartilhou no LocalSocial
	 */
	
	public static final ArrayList<String> listaArquivosComparilhados = new ArrayList<String>();
	
	public static int getCodigoUsuario() {
		return codigoUsuario;
	}
	
	public static String getApelidoUsuario() {
		return apelidoUsuario;
	}
	
	public static double getLatitudeUsuario() {
		return Parametros.latitudeUsuario;
	}
	
	public static double getLongitudeUsuario() {
		return Parametros.longitudeUsuario;
	}
	
	public static long getDataUltimaAtualizacaoPosicao() {
		return Parametros.dataUltimaAtualizacaoPosicao;
	}
	
	public static long getDataSincronizacaoUsuarios() {
		return Parametros.dataSincronizacaoUsuarios;
	}
	
	public static long getDataSincronizacaoMensagens() {
		return Parametros.dataSincronizacaoMensagens;
	}
	
	public static void setCodigoUsuario(int codigoUsuario) {
			Parametros.codigoUsuario = codigoUsuario;
	}
	
	public static void setApelidoUsuario(String apelidoUsuario) {
			Parametros.apelidoUsuario = apelidoUsuario;
	}
	
	public static void setPosicaoUsuario(int origem, double latitudeUsuario, double longitudeUsuario) {
			if( origem == 0 ) {
				Parametros.latitudeUsuario  = latitudeUsuario;
				Parametros.longitudeUsuario = longitudeUsuario;
				Parametros.origemAtualizacaoPosicao = 0;
				Parametros.dataUltimaAtualizacaoPosicao = System.currentTimeMillis();
				Log.v("GPS", "Atualizado posi��o via Satelite");					   
			}
			else {
				if( Parametros.origemAtualizacaoPosicao == 1 ) {
					Parametros.latitudeUsuario  = latitudeUsuario;
					Parametros.longitudeUsuario = longitudeUsuario;
					Parametros.origemAtualizacaoPosicao = 1;
					Parametros.dataUltimaAtualizacaoPosicao = System.currentTimeMillis();
					Log.v("GPS", "Atualizado posi��o via torre");
				}
				else
					if( Parametros.dataUltimaAtualizacaoPosicao + 60000 <= System.currentTimeMillis() ) {
						Parametros.latitudeUsuario  = latitudeUsuario;
						Parametros.longitudeUsuario = longitudeUsuario;
						Parametros.origemAtualizacaoPosicao = 1;
						Parametros.dataUltimaAtualizacaoPosicao = System.currentTimeMillis();
						Log.v("GPS", "Atualizado posi��o via torre");
					}
			}			
	}
	
	public static void atualizarDataSincronizacaoUsuarios() {
		Parametros.dataSincronizacaoUsuarios = System.currentTimeMillis();
	}
	
	public static void atualizarDataSincronizacaoMensagens() {
		Parametros.dataSincronizacaoMensagens = System.currentTimeMillis();
	}
}
