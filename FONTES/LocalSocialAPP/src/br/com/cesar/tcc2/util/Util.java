package br.com.cesar.tcc2.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.modelo.Conversa;

public final class Util {	
	
	public static String formatarDouble(double valor, String formato) {
		DecimalFormat df = new DecimalFormat(formato);  
		return df.format(valor); 
	}
	
	public String criptografarSenha(String senha) throws NoSuchAlgorithmException {
		MessageDigest m = MessageDigest.getInstance("MD5");
	  			  	  m.update(senha.getBytes(),0,senha.length());
		  
	    String valorMD5 = new BigInteger(1,m.digest()).toString(16);

	    while( valorMD5.length() < 32 )
	    	valorMD5 = "0" + valorMD5;
		  
	    return valorMD5;
	}
	
	public static String formatarDistancia(double distancia) {
		if( distancia > 1000 ) {
			 distancia = distancia / 1000D;
			 return formatarDouble(distancia,"####0.00" ) + "km";
		}
		else
			if( distancia < 1 ) {
				distancia = distancia * 100D;
				return formatarDouble(distancia, "###0.00") + "cm";
			}
			else
				return formatarDouble(distancia, "###0.00") + "m";
	}
	
	public static String formatarTamanhoArquivo(long tamanho) {
		String resultado = null;
		
		if( tamanho > 1024L ) {
			double tamanhoDouble = 0.0D;
			
			if( tamanho > 1048576 ) {
				tamanhoDouble = tamanho / 1048576d;
				resultado = formatarDouble(tamanhoDouble, "#0.00") + " mbytes";
			}
			else {
				tamanhoDouble = tamanho / 1024d;
				resultado = formatarDouble(tamanhoDouble, "##0.00") + " kbytes";				
			}				 
		}
		else
			resultado = String.valueOf(tamanho) + " bytes";
			
		return resultado;	
	}
	
	public static Arquivo[] recuperarArrayArquivos() {
		Arquivo[] arrayArquivos = null;
		
		if( Parametros.listaArquivosComparilhados != null && !Parametros.listaArquivosComparilhados.isEmpty() ) {
			arrayArquivos = new Arquivo[Parametros.listaArquivosComparilhados.size()];
			
			for( int indice = 0; indice < Parametros.listaArquivosComparilhados.size(); indice++ ) {
				String caminhoArquivo = Parametros.listaArquivosComparilhados.get(indice);
				
				File arquivo = new File(caminhoArquivo);
				
				arrayArquivos[indice] = new Arquivo(arquivo.getName(), arquivo.length(), caminhoArquivo);				
			}
		}
		
		return arrayArquivos;
	}
	
	public static void carregarArquivoParametros() throws IOException, Exception {
		File pastaAplicacao = new File(Parametros.CAMINHO_PASTA_APLICACAO);
		
		if( pastaAplicacao.exists() ) {
			pastaAplicacao = null;
			
			File arquivoConfiguracao = new File(Parametros.CAMINHO_ARQUIVO_CONFIGURACAO);
			
			if( arquivoConfiguracao.exists() ) {
				BufferedReader leitor = null;
				
				try {
					leitor = new BufferedReader(new FileReader(arquivoConfiguracao));
					
					String linha = null;
					
					while( (linha = leitor.readLine()) != null ) {
						if( linha.startsWith("URL_WEBSERVICE") )
							Parametros.urlWebService = linha.substring(linha.indexOf('=') + 1).trim();
						else
							if( linha.startsWith("PORTA_WEBSERVICE") )
								Parametros.portaWebService = Integer.parseInt(linha.substring(linha.indexOf('=') + 1).trim());
					}
				}
				finally {
					if( leitor != null )
						leitor.close();
				}
			}
		}
	}
	
	public static void gravarAlteracoes() throws IOException, Exception {
		File pastaLocalSocial = new File(Parametros.CAMINHO_PASTA_APLICACAO);
		File arquivoConf = new File(Parametros.CAMINHO_ARQUIVO_CONFIGURACAO);
			
		if( !pastaLocalSocial.exists() )
			pastaLocalSocial.mkdir();
			
		if( arquivoConf.exists() )
			arquivoConf.delete();
		
		BufferedWriter escritor = null;
		
		try {
			escritor = new BufferedWriter(new FileWriter(arquivoConf));
			escritor.write("URL_WEBSERVICE=" + Parametros.urlWebService);
			escritor.newLine();
			escritor.write("PORTA_WEBSERVICE=" + Parametros.portaWebService);
		}
		finally {
			if( escritor != null )
				escritor.close();
		}		
	}
	
	public static void gravarListaArquivos() throws IOException, Exception {
		File pastaLocalSocial = new File(Parametros.CAMINHO_PASTA_APLICACAO);
		File arquivoComp = new File(Parametros.CAMINHO_ARQUIVO_COMPARTILHADOS);
		
		if( !pastaLocalSocial.exists() )
			pastaLocalSocial.mkdir();
			
		if( arquivoComp.exists() )
			arquivoComp.delete();
		
		if( Parametros.listaArquivosComparilhados != null && !Parametros.listaArquivosComparilhados.isEmpty() ) {		
			BufferedWriter escritor = null;
		
			try {
				escritor = new BufferedWriter(new FileWriter(arquivoComp));
			
				for( int indice = 0; indice < Parametros.listaArquivosComparilhados.size(); indice++ ) {
					escritor.write(Parametros.listaArquivosComparilhados.get(indice));
				
					if( indice != Parametros.listaArquivosComparilhados.size() - 1 )
						escritor.newLine();
				}
			}
			finally {
				if( escritor != null )
					escritor.close();
			}
		}
	}
	
	public static void carregarArquivoCompartilhados() throws IOException, Exception {
		File pastaAplicacao = new File(Parametros.CAMINHO_PASTA_APLICACAO);
		
		if( pastaAplicacao.exists() ) {
			pastaAplicacao = null;
			
			File arquivoCompartilhados = new File(Parametros.CAMINHO_ARQUIVO_COMPARTILHADOS);
			
			if( arquivoCompartilhados.exists() ) {
				BufferedReader leitor = null;
				
				try {
					leitor = new BufferedReader(new FileReader(arquivoCompartilhados));
					ArrayList<String> listaLida = new ArrayList<String>();
					String linha = null;
					
					while( (linha = leitor.readLine()) != null )
						listaLida.add(linha);
					
					if( !listaLida.isEmpty() )
						for( String umArquivo : listaLida )
							Parametros.listaArquivosComparilhados.add(umArquivo);					
				}
				finally {
					if( leitor != null )
						leitor.close();
				}
			}
		}
	}
	
	public static Conversa[] recuperarArrayConversas() {
		Conversa[] arrayConversas = new Conversa[Parametros.hashConversas.size()];
		
		int indice = 0;
		
		for( int codigoUsuario : Parametros.hashConversas.keySet() ) {
			arrayConversas[indice] = Parametros.hashConversas.get(codigoUsuario);
			indice++;
		}
		
		return arrayConversas;
 	}
	
	
}
