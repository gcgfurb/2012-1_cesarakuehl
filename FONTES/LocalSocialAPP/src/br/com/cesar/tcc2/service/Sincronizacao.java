package br.com.cesar.tcc2.service;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import br.com.cesar.tcc2.AtividadeConversa;
import br.com.cesar.tcc2.AtividadePrincipal;
import br.com.cesar.tcc2.R;
import br.com.cesar.tcc2.modelo.Conversa;
import br.com.cesar.tcc2.modelo.Mensagem;
import br.com.cesar.tcc2.modelo.Usuario;
import br.com.cesar.tcc2.modelo.UsuarioWrapper;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public final class Sincronizacao extends Service implements Runnable {

	private boolean executar = true;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onDestroy() {
		this.executar = false;
	}
	
	@Override
	public void onCreate() {
		new Thread(this).start();
	}
	
	private void criarNotificacaoConversa(String mensagemBarraStatus, String titulo, String mensagem, int codigoUsuario) {
		int idNotification = R.string.app_name + codigoUsuario;
		
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification n = new Notification(R.drawable.ic_launcher, mensagemBarraStatus, System.currentTimeMillis());
				     n.flags = Notification.FLAG_AUTO_CANCEL;
				     
		
		Bundle parametros = new Bundle();
			   parametros.putInt("codigoUsuario", codigoUsuario);
			   
		Intent it = new Intent(this, AtividadeConversa.class);
			   it.putExtras(parametros);			   
		
		PendingIntent pi = PendingIntent.getActivity(this, 0, it , 0);
		
		n.setLatestEventInfo(this, titulo, mensagem, pi);
		nm.notify(idNotification, n);
		
		if( !Parametros.listaNotifications.contains(idNotification) )		
			Parametros.listaNotifications.add(idNotification);			
	}

	
	public void run() {
		LocalSocialWS lsws = new LocalSocialWS();
		
		while( this.executar ) {
			try {
				if( Parametros.getDataSincronizacaoUsuarios() + 3000 <= System.currentTimeMillis() && !Parametros.sincronizacaoEmAndamento ) {
					Parametros.sincronizacaoEmAndamento = true;
					
					Usuario[] arrayUsuarios = lsws.recuperarListaUsuarios(Parametros.getCodigoUsuario(), Parametros.distanciaComunicacao);
					
					if( !Parametros.sistemaEmPausa && arrayUsuarios != null ) {
						UsuarioWrapper uw = new UsuarioWrapper();
									   uw.setArrayUsuarios(arrayUsuarios);
						
						Intent it = new Intent(AtividadePrincipal.BROADCAST_LISTA_USUARIOS);
							   it.putExtra("listaUsuarios", uw);
							   
						sendBroadcast(it);
					}
					else
						if( arrayUsuarios != null ) {
							Conversa umaConversa = null;
							
							for( Usuario umUsuario : arrayUsuarios ) {
								umaConversa = Parametros.hashConversas.get(umUsuario.getCodigo());
								
								if( umaConversa != null )
									if( umaConversa.isAtiva() ) {
										Intent it = new Intent(AtividadeConversa.BROADCAST_DISTANCIA);
										   	   it.putExtra("distancia", umUsuario.getDistancia());
										   
										sendBroadcast(it);
										break;
									}
									else {
										umaConversa.setDistanciaUsuario(umUsuario.getDistancia());
										
										Intent it = new Intent(AtividadePrincipal.BROADCAST_LISTA_CONVERSAS);
										sendBroadcast(it);
									}	
							}
						}	
					
					Parametros.atualizarDataSincronizacaoUsuarios();
					Parametros.sincronizacaoEmAndamento = false;
				}
				
				if( Parametros.getDataSincronizacaoMensagens() + 1000 <= System.currentTimeMillis() ) {
					long inicio = System.currentTimeMillis();
					Mensagem[] arrayMensagens = lsws.recuperarMensagensUsuario(Parametros.getCodigoUsuario());
					
					if( arrayMensagens != null )
						for( Mensagem umaMensagem : arrayMensagens ) {
							Conversa umaConversa = Parametros.hashConversas.get(umaMensagem.getCodigoUsuario());
							
							if( umaMensagem.getMensagem().equals("/fim") ) {
								if( umaConversa != null && umaConversa.isAtiva() ) {
									Intent it = new Intent(AtividadeConversa.BROADCAST_MENSAGEM);
										   it.putExtra("msg", umaMensagem.getMensagem());
										   it.putExtra("distancia", umaMensagem.getDistancia());										   
								   
									sendBroadcast(it);	   
								}
								else {
									Parametros.hashConversas.remove(umaConversa.getCodigoUsuario());
									
									Intent it = new Intent(AtividadePrincipal.BROADCAST_LISTA_CONVERSAS);
									sendBroadcast(it);
								}	
							}
							else {
								if( umaConversa != null ) {
									if( umaConversa.isAtiva() ) {
										Intent it = new Intent(AtividadeConversa.BROADCAST_MENSAGEM);
										   	   it.putExtra("msg", umaMensagem.getLoginUsuario() + ": " + umaMensagem.getMensagem());
										   	   it.putExtra("distancia", umaMensagem.getDistancia());										   
										   
										   sendBroadcast(it);	   
									}
									else
										this.criarNotificacaoConversa("Nova Mensagem", "Nova mensagem", umaMensagem.getLoginUsuario() + ": " + umaMensagem.getMensagem(), umaMensagem.getCodigoUsuario());
								}
								else {
									umaConversa = new Conversa(umaMensagem.getCodigoUsuario(), umaMensagem.getLoginUsuario(), umaMensagem.getDistancia());
									Parametros.hashConversas.put(umaConversa.getCodigoUsuario(), umaConversa);
									
									Intent it = new Intent(AtividadePrincipal.BROADCAST_LISTA_CONVERSAS);
									sendBroadcast(it);
									
									this.criarNotificacaoConversa("Nova Mensagem", "Nova mensagem", umaMensagem.getLoginUsuario() + ": " + umaMensagem.getMensagem(), umaMensagem.getCodigoUsuario());
								}
							
								umaConversa.addLinhaConversa(umaMensagem.getLoginUsuario() + ": " + umaMensagem.getMensagem());
							}
						}
					
					Parametros.atualizarDataSincronizacaoMensagens();
					
					if( arrayMensagens != null )
						Log.v("Mensagens", String.valueOf(System.currentTimeMillis() - inicio));
				}
				
				Thread.sleep(1000);
			}
			catch(IOException io) {
				if( !Parametros.sistemaEmPausa ) {
					Intent it = new Intent(AtividadePrincipal.BROADCAST_ERROS);
				       	   it.putExtra("erro", "Falha na serviço de sincronização: IOException - " + io.getMessage());
				
				    sendBroadcast(it);
				}    
				       
				Parametros.sincronizacaoEmAndamento = false;
			}
			catch(XmlPullParserException xppe) {
				if( !Parametros.sistemaEmPausa ) {
					Intent it = new Intent(AtividadePrincipal.BROADCAST_ERROS);
			           	   it.putExtra("erro", "Falha na serviço de sincronização: XmlPullParserException - " + xppe.getMessage());
			      
			        sendBroadcast(it);
				}    

			    Parametros.sincronizacaoEmAndamento = false;
			}
			catch(InterruptedException ie) {
				if( !Parametros.sistemaEmPausa ) {				
					Intent it = new Intent(AtividadePrincipal.BROADCAST_ERROS);
		           	   	   it.putExtra("erro", "Falha na serviço de sincronização: InterruptedException - " + ie.getMessage());
		      
		            sendBroadcast(it);
				}    
				
				Parametros.sincronizacaoEmAndamento = false;
			}
			catch(Exception e) {
				if( !Parametros.sistemaEmPausa ) {
					Intent it = new Intent(AtividadePrincipal.BROADCAST_ERROS);
	           	   	   	   it.putExtra("erro", "Falha na serviço de sincronização: Exception - " + e.getMessage());
	      
	           	    sendBroadcast(it);
				}    
				
				Parametros.sincronizacaoEmAndamento = false;
			}
		}
	}
	
}
