package br.com.cesar.tcc2.service;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.IBinder;
import br.com.cesar.tcc2.AtividadePrincipal;
import br.com.cesar.tcc2.listener.LocationListenerNetwork;
import br.com.cesar.tcc2.listener.LocationListenerSatelite;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.ws.LocalSocialWS;

public final class Georastreamento extends Service implements Runnable {
	
	private LocationListener mlocListenerSAT = null;
	private LocationListener mlocListenerNET = null;
	private LocationManager  mlocManagerSAT  = null;
	private LocationManager  mlocManagerNET  = null;
	
	private boolean executar = true;
	
	@Override
	public void onCreate() {
		this.mlocListenerSAT = new LocationListenerSatelite();
		this.mlocManagerSAT  = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		this.mlocManagerSAT.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this.mlocListenerSAT);
		
		this.mlocListenerNET = new LocationListenerNetwork();
		this.mlocManagerNET = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		this.mlocManagerNET.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this.mlocListenerNET);
		
		new Thread(this).start();
	}
	
	@Override
	public void onDestroy() {
		this.executar = false;
		this.mlocManagerSAT.removeUpdates(this.mlocListenerSAT);
		this.mlocManagerNET.removeUpdates(this.mlocListenerNET);
	}	
	
	public void run() {
		long ultimaDataTransmissao = Parametros.getDataUltimaAtualizacaoPosicao();		
	
		while(this.executar) {		
			try {
				if( this.executar && ultimaDataTransmissao != Parametros.getDataUltimaAtualizacaoPosicao() ) {				
					LocalSocialWS lsws = new LocalSocialWS();
    					  	  	  lsws.atualizarPosicaoUsuario(Parametros.getCodigoUsuario(), Parametros.getLatitudeUsuario(), Parametros.getLongitudeUsuario());
				
    			    ultimaDataTransmissao = Parametros.getDataUltimaAtualizacaoPosicao();
				}
				
				Thread.sleep(1000);
			}
			catch(IOException io) {
				Intent it = new Intent(AtividadePrincipal.BROADCAST_ERROS);
					   it.putExtra("erro", "Falha ao atualizar sua posi��o no sistema: IOException - " + io.getMessage());
					   
				sendBroadcast(it);	   
			}
			catch(XmlPullParserException xppe) {
				Intent it = new Intent(AtividadePrincipal.BROADCAST_ERROS);
				   	   it.putExtra("erro", "Falha ao atualizar sua posi��o no sistema: XmlPullParserException - " + xppe.getMessage());
				   
				sendBroadcast(it);
			}
			catch(InterruptedException ie) {
				Intent it = new Intent(AtividadePrincipal.BROADCAST_ERROS);
			   	   	   it.putExtra("erro", "Falha ao atualizar sua posi��o no sistema: InterruptedException - " + ie.getMessage());
			   
			   	sendBroadcast(it);
			}
		}	
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
}
