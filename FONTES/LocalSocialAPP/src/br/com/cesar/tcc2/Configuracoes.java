package br.com.cesar.tcc2;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.com.cesar.tcc2.util.Parametros;
import br.com.cesar.tcc2.util.Util;

public class Configuracoes extends Activity implements OnClickListener {
	
	private EditText edUrlWebService   = null,
			 		 edPortaWebService = null;
		
	private void recuperarDadosConfiguracao() {
		try {
			Util.carregarArquivoParametros();
			
			this.edUrlWebService.setText(Parametros.urlWebService);
			this.edPortaWebService.setText(String.valueOf(Parametros.portaWebService));
		}
		catch(IOException io) {
			Toast.makeText(Configuracoes.this, "Falha ao carregar arquivo de configura��o: " + io.getMessage(), Toast.LENGTH_LONG).show();
		}
		catch(Exception e) {
			Toast.makeText(Configuracoes.this, "Falha ao carregar arquivo de configura��o: " + e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	private void restaurarPadrao() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
							builder.setMessage("Deseja restaurar as configura��es padr�es?");
							builder.setCancelable(false);
							builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			
								public void onClick(DialogInterface dialog, int id) {
									Parametros.urlWebService = Parametros.URL_WEBSERVICE;
									Parametros.portaWebService = Parametros.PORTA_WEBSERVICE;
									
									edPortaWebService.setText(String.valueOf(Parametros.PORTA_WEBSERVICE));
									edUrlWebService.setText(Parametros.URL_WEBSERVICE);
									
									File arquivoConf = new File(Parametros.CAMINHO_ARQUIVO_CONFIGURACAO);
										 arquivoConf.delete();										 
								}
							});
		
							builder.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
		
		AlertDialog alert = builder.create();
					alert.show();	
	}
	
	private boolean validarInsercoesInformacoes() {
		boolean sucesso = true;
		
		if( this.edUrlWebService.getText().toString() == null || this.edUrlWebService.getText().toString().equals("") ) {
			sucesso = false;
			Toast.makeText(Configuracoes.this, "Voc� deve informar a URL do WEBService do LocalSocial", Toast.LENGTH_LONG).show();
			this.edUrlWebService.requestFocus();
		}
		else
			if( this.edPortaWebService.getText().toString() == null || this.edPortaWebService.getText().toString().equals("") ) {
				sucesso = false;
				Toast.makeText(Configuracoes.this, "Voc� deve informar a porta do WEBService do LocalSocial", Toast.LENGTH_LONG).show();
				this.edPortaWebService.requestFocus();
			}
					
		return sucesso;
	}
	
	private boolean ocorreuAlteracao() {
		return ( (!this.edUrlWebService.getText().toString().equals(Parametros.urlWebService)) ||  
				 (!String.valueOf(Parametros.portaWebService).equals(this.edPortaWebService.getText().toString()))
			   );
	}
	
	private void gravarAlteracoes() {
		if( this.validarInsercoesInformacoes() && this.ocorreuAlteracao() ) {			
			File arquivoConf = new File(Parametros.CAMINHO_ARQUIVO_CONFIGURACAO);
			
			try {
				Parametros.urlWebService   = this.edUrlWebService.getText().toString().trim();
				Parametros.portaWebService = Integer.parseInt(this.edPortaWebService.getText().toString().trim());
				
				Util.gravarAlteracoes();				
				Toast.makeText(Configuracoes.this, "Parametros atualizados com sucesso", Toast.LENGTH_LONG).show();
			}
			catch(IOException io) {
				Toast.makeText(Configuracoes.this, "Erro na grava��o dos parametros para o cart�o SD: " + io.getMessage(), Toast.LENGTH_LONG).show();				
				arquivoConf.delete();
				
				Parametros.portaWebService = Parametros.PORTA_WEBSERVICE;
				Parametros.urlWebService   = Parametros.URL_WEBSERVICE;
				
				this.edPortaWebService.setText(String.valueOf(Parametros.PORTA_WEBSERVICE));
				this.edUrlWebService.setText(Parametros.URL_WEBSERVICE);
			}
			catch(Exception e) {
				Toast.makeText(Configuracoes.this, "Erro na grava��o dos parametros para o cart�o SD: " + e.getMessage(), Toast.LENGTH_LONG).show();
				arquivoConf.delete();
				
				Parametros.portaWebService = Parametros.PORTA_WEBSERVICE;
				Parametros.urlWebService   = Parametros.URL_WEBSERVICE;
				
				this.edPortaWebService.setText(String.valueOf(Parametros.PORTA_WEBSERVICE));
				this.edUrlWebService.setText(Parametros.URL_WEBSERVICE);
			}			
		}
		
		finish();
	}
	
	private void inicializarComponentes() {
		this.edUrlWebService   = (EditText) findViewById(R.id.confEtHostLocalSocial);
		this.edPortaWebService = (EditText) findViewById(R.id.confEtPortaLocalSocial);
		
		Button botaoPadrao = (Button) findViewById(R.id.confBtPadrao);
		   	   botaoPadrao.setOnClickListener(this);
		   
		Button botaoOK = (Button) findViewById(R.id.confBtOK);
		   	   botaoOK.setOnClickListener(this);		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.tela_config);
		
		this.inicializarComponentes();				
		this.recuperarDadosConfiguracao();	
	}
	
	public void onClick(View v) {
		switch( v.getId() ) {
			case R.id.confBtOK:     this.gravarAlteracoes();
									break;
			
			case R.id.confBtPadrao: this.restaurarPadrao();
								    break;
		}
	}
	
}
