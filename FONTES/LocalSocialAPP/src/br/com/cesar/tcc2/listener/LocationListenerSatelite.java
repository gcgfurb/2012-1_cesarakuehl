package br.com.cesar.tcc2.listener;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import br.com.cesar.tcc2.util.Parametros;

public final class LocationListenerSatelite implements LocationListener {
	
	public void onLocationChanged(Location location) {
		Parametros.setPosicaoUsuario(0, location.getLatitude(), location.getLongitude());
	}
	
	public void onProviderDisabled(String provider) {}
	public void onProviderEnabled(String provider) {}
	public void onStatusChanged(String provider, int status, Bundle extras) {}
}
