package br.com.cesar.tcc2.listener;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import br.com.cesar.tcc2.util.Parametros;

public final class LocationListenerNetwork implements LocationListener {

	public void onLocationChanged(Location location) {
		Parametros.setPosicaoUsuario(1, location.getLatitude(), location.getLongitude());
	}

	public void onProviderDisabled(String arg0) {}
	public void onProviderEnabled(String arg0) {}
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}
}
