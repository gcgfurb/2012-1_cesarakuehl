package br.com.cesar.tcc2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import br.com.cesar.tcc2.adapter.ArquivoAdapter;
import br.com.cesar.tcc2.modelo.Arquivo;
import br.com.cesar.tcc2.modelo.ArquivoWrapper;
import br.com.cesar.tcc2.task.DownloadArquivo;
import br.com.cesar.tcc2.util.Parametros;

public class ArquivosUsuario extends Activity implements OnItemClickListener {
	
	private String nomeUsuario = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.atividade_arq_usuario);
		
		Intent it = getIntent();
		
		this.nomeUsuario = it.getStringExtra("usuario");
		
		TextView tv = (TextView) findViewById(R.id.arqusuarioTVTitulo);
				 tv.setText("Arquivos compartilhados de " + this.nomeUsuario);
				 
		ArquivoWrapper aw = (ArquivoWrapper) it.getSerializableExtra("arquivos");
		
		ListView lv = (ListView) findViewById(R.id.arqusuarioLVArquivos);
				 lv.setAdapter(new ArquivoAdapter(this, aw.getArrayArquivos()));
				 lv.setOnItemClickListener(this);
	}

	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		final Arquivo umArquivo = (Arquivo) arg0.getItemAtPosition(arg2);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(ArquivosUsuario.this);
				builder.setMessage("Deseja realizar o download do arquivo " + umArquivo.getNome() + "?");
				builder.setCancelable(false);
				builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						String urlArquivo = "http://" + Parametros.urlWebService + ":" + Parametros.portaWebService + "/" + umArquivo.getCaminho();
						new DownloadArquivo(ArquivosUsuario.this, urlArquivo, umArquivo.getNome(), nomeUsuario).execute();						
					}	        						
				});

				builder.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		AlertDialog alert = builder.create();
					alert.show();
		
	}	
}
