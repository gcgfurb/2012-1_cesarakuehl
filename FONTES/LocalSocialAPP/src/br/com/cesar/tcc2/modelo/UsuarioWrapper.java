package br.com.cesar.tcc2.modelo;

public class UsuarioWrapper implements java.io.Serializable {
	
	private static final long serialVersionUID = 8530548992372871283L;
	
	private Usuario[] arrayUsuarios = null;
	
	public Usuario[] getArrayUsuarios() {
		return this.arrayUsuarios;
	}
	
	public void setArrayUsuarios(Usuario[] arrayUsuarios) {
		this.arrayUsuarios = arrayUsuarios;
	}
	
}
