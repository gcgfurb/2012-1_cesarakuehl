package br.com.cesar.tcc2.modelo;

import java.io.Serializable;

public class ArquivoWrapper implements Serializable {
	
	private static final long serialVersionUID = 5462423978431849678L;
	
	private Arquivo[] arrayArquivos = null;
	
	public ArquivoWrapper(Arquivo[] arrayArquivos) {
		this.arrayArquivos = arrayArquivos;
	}
	
	public Arquivo[] getArrayArquivos() {
		return this.arrayArquivos;
	}
	
}
