package br.com.cesar.tcc2.modelo;

import java.io.Serializable;
import java.util.ArrayList;

public final class Conversa implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private final ArrayList<String> historicoConversa = new ArrayList<String>();
	
	private int codigoUsuario = -1;
	
	private String apelidoUsuario = null;
	
	private double distanciaUsuario = 0D;
	
	private boolean ativa = false;
	
	public Conversa(int codigoUsuario, String apelidoUsuario, double distanciaUsuario) {
		this.codigoUsuario = codigoUsuario;
		this.apelidoUsuario = apelidoUsuario;
		this.distanciaUsuario = distanciaUsuario;
	}
	
	public boolean isAtiva() {
		return this.ativa;
	}
	
	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}
	
	public int getCodigoUsuario() {
		return this.codigoUsuario;
	}
	
	public String getApelidoUsuario() {
		return this.apelidoUsuario;
	}
	
	public double getDistanciaUsuario() {
		return this.distanciaUsuario;
	}
	
	public void setDistanciaUsuario(double distanciaUsuario) {
		this.distanciaUsuario = distanciaUsuario;
	}
	
	public void addLinhaConversa(String linha) {
		this.historicoConversa.add(linha);
	}
	
	public ArrayList<String> getHistoricoConversa() {
		return this.historicoConversa;
	}
}
