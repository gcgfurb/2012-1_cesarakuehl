package br.com.cesar.tcc2.modelo;

import java.io.Serializable;

public final class Arquivo implements Serializable {
	
	private static final long serialVersionUID = 4479755469128441904L;

	private String nome = null;
	
	private long tamanho = 0L;
	
	private String caminho = null;
	
	public Arquivo(String nome, long tamanho, String caminho) {
		this.nome = nome;
		this.tamanho = tamanho;
		this.caminho = caminho;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public long getTamanho() {
		return this.tamanho;
	}
	
	public String getCaminho() {
		return this.caminho;
	}
	
}
